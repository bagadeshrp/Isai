package com.baga.isai.parsers

import android.database.Cursor
import android.provider.MediaStore
import com.baga.isai.database.DBConstants
import com.baga.isai.models.Playlist
import com.baga.isai.models.Song
import com.baga.isai.utils.GeneralUtils

object MusicCursorParser
{
    fun parse(lCursor: Cursor?) : ArrayList<Song>
    {
        val list = ArrayList<Song> ()

        if (lCursor==null || lCursor.count<1)
        {
            return list
        }
        var num=1
        lCursor.moveToFirst()
        do{
            val number = num++
            val title = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
            val artist = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
            val album = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
            val duration = GeneralUtils.getTimeInMMSS( lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)).toLong()  )
            val durationInMs = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)).toLong()
            val path = lCursor.getString(lCursor.getColumnIndex(MediaStore.MediaColumns.DATA))
            val key = lCursor.getString(lCursor.getColumnIndex(MediaStore.MediaColumns._ID))

            list.add(Song(number, title, artist, path, key, duration, album,durationInMs))
        }while (lCursor.moveToNext())
        return list
    }


    fun parsePlaylist(lCursor: Cursor?) : ArrayList<Playlist>
    {
        val list = ArrayList<Playlist> ()

        if (lCursor==null || lCursor.count<1)
        {
            return list
        }
        lCursor.moveToFirst()
        do{
            val id = lCursor.getInt (lCursor.getColumnIndex(DBConstants.PlaylistColumns.id))
            val title = lCursor.getString(lCursor.getColumnIndex(DBConstants.PlaylistColumns.title))
            val songCount = lCursor.getInt(lCursor.getColumnIndex(DBConstants.PlaylistColumns.songCount))
            val duration = lCursor.getString(lCursor.getColumnIndex(DBConstants.PlaylistColumns.totalDuration))
            list.add(Playlist(id,title,songCount,duration))
        }while (lCursor.moveToNext())
        return list
    }

//    fun parsePlaylist(lCursor: Cursor?) : ArrayList<Playlist>
//    {
//        val list = ArrayList<Playlist> ()
//
//        if (lCursor==null || lCursor.count<1)
//        {
//            return list
//        }
//        lCursor.moveToFirst()
//        do{
//            val id = lCursor.getInt (lCursor.getColumnIndex(DBConstants.PlaylistColumns.id))
//            val title = lCursor.getString(lCursor.getColumnIndex(DBConstants.PlaylistColumns.title))
//            val songCount = lCursor.getInt(lCursor.getColumnIndex(DBConstants.PlaylistColumns.songCount))
//            val duration = lCursor.getString(lCursor.getColumnIndex(DBConstants.PlaylistColumns.totalDuration))
//            list.add(Playlist(id,title,songCount,duration))
//        }while (lCursor.moveToNext())
//        return list
//    }


}