package com.baga.isai.datapacks

import com.baga.isai.R
import com.baga.isai.navigation.NavigationItemModel
import com.baga.isai.navigation.NavigationType

object InitialSetupDatas
{
    fun getNavigationViewItems() : ArrayList<NavigationItemModel>
    {
        val list  = ArrayList<NavigationItemModel>()
        list.add(NavigationItemModel("Current",NavigationType.SONGTYPE,image = R.drawable.ic_pausebutton))
        list.add(NavigationItemModel("All Songs",NavigationType.SONGTYPE,image = R.drawable.ic_musicicon))
        list.add(NavigationItemModel("Albums",NavigationType.SONGTYPE,image = R.drawable.ic_albumicon))
        list.add(NavigationItemModel("Artists",NavigationType.SONGTYPE,image = R.drawable.ic_micicon))
        list.add(NavigationItemModel("Genres",NavigationType.SONGTYPE,image = R.drawable.ic_genreicon))
        list.add(NavigationItemModel("Playlists",NavigationType.SONGTYPE,image = R.drawable.ic_playlisticon))
        list.add(NavigationItemModel("Folders",NavigationType.SONGTYPE,image = R.drawable.ic_foldericon))
        return list
    }

    /**
     * settings, buy now...
     */
    fun getNavigationViewStaticItems() : ArrayList<NavigationItemModel>
    {
        val list  = ArrayList<NavigationItemModel>()
        list.add(NavigationItemModel("Buy now",NavigationType.SPECIAL,image = R.drawable.ic_pausebutton))
        list.add(NavigationItemModel("Settings",NavigationType.SETTINGSTYPE,image = R.drawable.ic_settingsicon))
        list.add(NavigationItemModel("Stellio Store",NavigationType.SETTINGSTYPE,image = R.drawable.ic_storeicon))
        list.add(NavigationItemModel("Equalizer",NavigationType.SETTINGSTYPE,image = R.drawable.ic_equalizericon))

        return list
    }
}