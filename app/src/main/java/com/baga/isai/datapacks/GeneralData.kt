package com.baga.isai.datapacks

import com.baga.isai.R
import com.baga.isai.models.Musicoption

object GeneralData
{
    fun getOptionData() : ArrayList<Musicoption>
    {
        val list : ArrayList<Musicoption> = ArrayList()
        list.add( Musicoption("Lyrics", R.drawable.ic_playlisticon) )
        list.add( Musicoption("Edit tags", R.drawable.ic_playbutton2) )
        list.add( Musicoption("Go to artist", R.drawable.ic_genreicon) )
        list.add( Musicoption("Go to album", R.drawable.ic_albumicon))
        list.add( Musicoption("Set as ringtone", R.drawable.ic_settingsicon) )
        list.add( Musicoption("Share", R.drawable.ic_share) )
        list.add( Musicoption("Delete", R.drawable.ic_repeatbutton1) )

        return list
    }
}