package com.baga.isai.datapacks

import android.graphics.Color
import kotlin.random.Random

object ColorDatas
{
    val list  = ArrayList<Int>()

    init {

        list.add(Color.parseColor("#E0115F"))
        list.add(Color.parseColor("#7d8085"))


    }

    val randomColor = list.get(Random.nextInt(list.size)+0)
}