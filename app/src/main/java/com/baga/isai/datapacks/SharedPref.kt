package com.baga.isai.datapacks

import com.baga.isai.AppController

class SharedPref
{

    companion object
    {
        val SHARED_PREF_NAME = "BagaIsai"
        val CURRENT_SELECTED_MENU = "CurrentlySelected"

        // theme
        val SELECTED_THEME = "SelectedTheme"

        fun getString(key : String, default : String = "") : String
        {
            val prefs = AppController.getAppContext().getSharedPreferences(SHARED_PREF_NAME,0)
            return prefs.getString(key,default)
        }
        fun getBoolean(key : String, default : Boolean = false) : Boolean
        {
            val prefs = AppController.getAppContext().getSharedPreferences(SHARED_PREF_NAME,0)
            return prefs.getBoolean(key,default)
        }
        fun put(key : String , value : String)
        {
            val prefsEdit = AppController.getAppContext().getSharedPreferences(SHARED_PREF_NAME,0).edit()
            prefsEdit.putString(key,value)
            prefsEdit.apply()
        }
        fun put(key : String , value : Boolean)
        {
            val prefsEdit = AppController.getAppContext().getSharedPreferences(SHARED_PREF_NAME,0).edit()
            prefsEdit.putBoolean(key,value)
            prefsEdit.apply()
        }

    }

    interface Keys{
        companion object{
            val PLAY_NEXT_SONG = "ContinuePlayingNextSong"

        }
    }

}