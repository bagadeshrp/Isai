package com.baga.isai.datapacks

import com.baga.isai.database.DatabaseUtil
import com.baga.isai.models.Album
import com.baga.isai.models.Song
import com.baga.isai.parsers.MusicCursorParser

object Converters
{
    fun getSongsFromAlbum(albumId : String) : ArrayList<Song>
    {
        val cursor =   DatabaseUtil.getSongsFromAlbum(albumId)
        return MusicCursorParser.parse(cursor)
    }
}