package com.baga.isai.navigation

import com.baga.isai.R

enum class NavigationType
{
   SONGTYPE, SPECIAL, SETTINGSTYPE
}

data class NavigationItemModel (var displayValue : String , val type : NavigationType , val image : Int = R.drawable.ic_menuicon )
{

}