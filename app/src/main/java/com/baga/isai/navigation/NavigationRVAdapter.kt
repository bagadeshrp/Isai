package com.baga.isai.navigation

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.R
import com.baga.isai.datapacks.SharedPref
import com.baga.isai.datapacks.SharedPref.Companion.CURRENT_SELECTED_MENU
import com.baga.isai.presenters.ColorPresenter
import com.baga.isai.presenters.InitialSetupPresenter

class NavigationRVAdapter(val mContext: Context, val data: ArrayList<NavigationItemModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    var currentSelectedTitle : String = ""

    init {
        currentSelectedTitle = SharedPref.getString(CURRENT_SELECTED_MENU, default = "Current");
        data.addAll(InitialSetupPresenter.getNavigationViewStaticItem())
    }

    var lastSelected = -1
    var previousHolder : NaviViewHolder ? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.navi_item_view,parent,false)
        val holder = NaviViewHolder(view)
        holder.cardView.setOnClickListener{
            onClick(holder)
        }

        return holder
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder = holder as NaviViewHolder
        val item = data.get(position)
        holder.title.text = data.get(position).displayValue
        holder.image.setImageResource(item.image)
        holder.cardView.setTag(item.type)
        if(item.type==NavigationType.SPECIAL)
        {
            val lparms =  holder.cardView.layoutParams as RecyclerView.LayoutParams
            lparms.topMargin = 200
            lparms.bottomMargin = 50
            holder.cardView.setCardBackgroundColor(mContext.getColor(R.color.BuyNowColor))
        }
        else if(item.type == NavigationType.SETTINGSTYPE)
        {
            holder.title.setTextColor(mContext.getColor(R.color.greyColor))
        }
        else if(item.type == NavigationType.SPECIAL)
        {

            val tValue = TypedValue()
            mContext.theme.resolveAttribute(android.R.attr.selectableItemBackground,tValue,true)
            holder.cardView.setBackgroundResource(tValue.resourceId)
        }


        if(item.displayValue == currentSelectedTitle )
        {
            onClick(holder)
        }

    }

    class NaviViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView)
    {
        val title = itemView.findViewById<TextView>(R.id.naviItemTitle)
        val image = itemView.findViewById<ImageView>(R.id.naviItemImageView)
        val cardView = itemView.findViewById<CardView>(R.id.naviItemCardView)
    }


    @SuppressLint("NewApi")
    fun onClick(holder : NaviViewHolder)
    {
        val type = holder.cardView.tag as NavigationType

        if ( type == NavigationType.SONGTYPE || type==NavigationType.SETTINGSTYPE)
        {
            clearPreviousSelected()
            holder.cardView.setCardBackgroundColor(ColorPresenter.getColorNavigationColor())
            holder.title.setTextColor(Color.WHITE)
            holder.cardView.cardElevation = 20F
            lastSelected = holder.adapterPosition
            previousHolder = holder
            currentSelectedTitle = holder.title.text.toString()
            SharedPref.put(CURRENT_SELECTED_MENU,currentSelectedTitle)
        }
        else if (type == NavigationType.SPECIAL)
        {

        }

    }

    @SuppressLint("NewApi")
    fun clearPreviousSelected()
    {
        var type = previousHolder?.cardView?.tag
        if(type==null)
        {
            return
        }
        type = type as NavigationType

        if (type ==  NavigationType.SONGTYPE  )
        {
            previousHolder?.cardView?.setCardBackgroundColor( Color.WHITE)
            previousHolder?.cardView?.cardElevation = 0F
            previousHolder?.title?.setTextColor( Color.BLACK )
        }
        else if ( type == NavigationType.SPECIAL)
        {
            // buy now
            previousHolder?.cardView?.setCardBackgroundColor( mContext.getColor(R.color.BuyNowColor) )
            previousHolder?.cardView?.cardElevation = 0F
            previousHolder?.title?.setTextColor( Color.BLACK )
        }
        else if(type==NavigationType.SETTINGSTYPE)
        {
            previousHolder?.cardView?.setCardBackgroundColor( Color.WHITE)
            previousHolder?.cardView?.cardElevation = 0F
            previousHolder?.title?.setTextColor( mContext.getColor(R.color.greyColor) )

        }

    }
}