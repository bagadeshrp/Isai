package com.baga.isai.customcomp

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PreCachingLayoutManager : LinearLayoutManager
{
    private val context : Context
    private val defaultLayoutSpace = 600
    private var extraLayoutSpace = -1
    constructor(context: Context) : super(context)
    {
        this.context= context
    }
    constructor(context: Context,exLayoutSpace : Int):super(context)
    {
        this.context= context
        this.extraLayoutSpace = exLayoutSpace
    }
    constructor(context: Context, orientation: Int, reverseLayout: Boolean) : super(
        context,
        orientation,
        reverseLayout
    )
    {
        this.context = context
    }

    fun setExtraLayoutSpace( extraLayoutSpace : Int )
    {
        this.extraLayoutSpace = extraLayoutSpace
    }

    override fun getExtraLayoutSpace(state: RecyclerView.State?): Int {
        return if(extraLayoutSpace>0) {
            extraLayoutSpace
        }
        else defaultLayoutSpace
    }



}