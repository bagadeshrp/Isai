package com.baga.isai.customcomp

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class BKRecyclerview : RecyclerView
{
    override fun isInEditMode(): Boolean {
        return true
    }
    constructor(context: Context): super(context){

    }
    constructor(context: Context, attributeSet: AttributeSet): super(context,attributeSet) {

    }
    constructor(context: Context, attributeSet: AttributeSet, defStyle : Int): super(context,attributeSet,defStyle) {

    }

}