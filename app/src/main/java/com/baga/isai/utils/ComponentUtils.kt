package com.baga.isai.utils

import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import com.baga.isai.AppController
import com.baga.isai.models.Song
import com.baga.isai.services.MusicServices

object ComponentUtils
{
    fun startMusicService(context: Context, intent: Intent = Intent(context,MusicServices::class.java).setAction(MusicServices.INIT))
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        }
        else
        {
            context.startService(intent)
        }
    }
    fun startMusicServiceWithConnection(context: Context, intent: Intent = Intent(context,MusicServices::class.java).setAction(MusicServices.INIT) , connection: ServiceConnection)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        }
        else
        {
            context.startService(intent)
        }
    }
    fun stopMusicServices(context: Context  = AppController.getAppContext(), intent: Intent = Intent(context,MusicServices::class.java).setAction(MusicServices.PAUSE))
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        }
        else
        {
            context.startService(intent)
        }
    }

    fun startMusic(context: Context = AppController.getAppContext(), song : Song)
    {
        val intent: Intent = Intent(context, MusicServices::class.java).setAction(MusicServices.PLAY)
        intent.putExtra(MusicServices.DATA_PATH,song.path)
        intent.putExtra(MusicServices.DATA_KEY,song.key)
        intent.putExtra(MusicServices.NAME,song.title)
        intent.putExtra(MusicServices.ALBUM,song.album)
        intent.putExtra(MusicServices.ARTIST,song.artist)
        startMusicService(context,intent)
    }

}