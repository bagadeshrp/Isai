package com.baga.isai.utils

import com.baga.isai.AppController
import com.baga.isai.models.Song


class SongUtils (val songList : ArrayList<Song>)
{
    companion object {
        private var sIntance : SongUtils? = null

        fun getSingleton( list : ArrayList<Song>) : SongUtils {
            if (sIntance == null )
            {
                sIntance = SongUtils(list)
                return sIntance!!
            }
            return sIntance!!
        }



    }

    fun getPreviousSong(song: Song) : Song?
    {
        val currentSongIndex = songList.indexOf(song)

        if (currentSongIndex!=-1)
        {
            // valid
            if (currentSongIndex-1 >= 0)
            {
                // valid previous song
                return songList[currentSongIndex-1]

            }
        }
        return null
    }
    fun getNextSong(song: Song) : Song?
    {
        val currentSongIndex = songList.indexOf(song)

        if (currentSongIndex!=-1)
        {
            // valid
            if (currentSongIndex+1 < songList.size)
            {
                // valid previous song
                return songList[currentSongIndex+1]

            }
        }
        return null
    }




}
