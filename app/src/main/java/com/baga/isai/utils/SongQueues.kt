package com.baga.isai.utils

import com.baga.isai.models.Song

class SongQueues{

    companion object{
        private var sIntance : SongQueues ? = null

        fun getSingleton() : SongQueues {
            if (sIntance == null )
            {
                sIntance = SongQueues()
                return sIntance!!
            }
            return sIntance!!
        }
    }
    lateinit var  queueOfSongs : ArrayList<Song>

    fun getNextSong( currentSong : Song) : Song
    {
        val index = queueOfSongs.indexOf(currentSong)
        if (index >= 0 && index+1 < queueOfSongs.size ){
            return queueOfSongs[index+1]

        }
        throw ArrayIndexOutOfBoundsException()
    }

}
