package com.baga.isai.utils

import android.Manifest
import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object GeneralUtils
{
    /**
     * pass long value
     * returns time in XX:04:09 format
     */
    fun getTimeInMMSS(value : Long) : String
    {
        val time = StringBuilder()
        try {
            val hour = TimeUnit.MILLISECONDS.toHours(value)
            val min = TimeUnit.MILLISECONDS.toMinutes(value) - TimeUnit.HOURS.toMinutes(hour)
            val sec = TimeUnit.MILLISECONDS.toSeconds(value) - TimeUnit.MINUTES.toSeconds(min)
            if(hour!=0L)
            {
                if (hour<10)
                {
                    time.append("0")
                }
                time.append(hour.toString())
                    .append(":")
            }
            if(min==0L)
            {
                time.append("00")
                    .append(":")
            }
            else
            {
                if (min<10)
                {
                    time.append("0")
                }
                time.append(min.toString())
                    .append(":")
            }
            if(sec==0L)
            {
                time.append("00")
            }
            else
            {
                if (sec<10)
                {
                    time.append("0")
                }
                time.append(sec.toString())
            }
            return time.toString()
        }
        catch (e : Exception){
            e.printStackTrace()
        }
        return ""
    }

    /**
     * @return true when app has storage permision
     */
    fun checkStoragePermission(context : Context) : Boolean
    {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)  == PERMISSION_GRANTED
    }
}