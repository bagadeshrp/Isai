package com.baga.isai.utils

import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.baga.isai.R

/**
 *
 */
object TransactionUtils
{

    fun performTransaction( fm : FragmentManager ,  tag : String ,  fragment : Fragment, layoutId : Int = R.id.mainactivityFLyaout)
    {


        val newBackStackLen = fm.backStackEntryCount + 1
        fm.beginTransaction()
            .replace(layoutId,fragment,tag)
            .addToBackStack(tag)
            .commit()

        // onBackpressed in activity as checks for any fragment and calls popimmediate
        fm.addOnBackStackChangedListener {
            val newCount = fm.backStackEntryCount
            if(newCount>1)
            {
                // remove toolbar
                
            }
            if (newBackStackLen!=newCount)
            {
                fm.removeOnBackStackChangedListener{}
                if (newBackStackLen>newCount)
                {
                    // back is pressed
                }
            }
        }
    }

    fun performTransaction( fm : FragmentManager ,  tag : String ,  fragment : Fragment,  listOfElement : ArrayList<View> ? ,  listOfElementTransName: ArrayList<String>?,layoutId : Int = R.id.mainactivityFLyaout)
    {
        val newBackStackLen = fm.backStackEntryCount + 1
        val ft = fm.beginTransaction();



        if (listOfElement !=null && listOfElementTransName!=null &&  listOfElement.size>0)
        {
            ft.addSharedElement(listOfElement[0],listOfElementTransName[0])
            ft.add(layoutId,fragment,tag)

        }
        else
        {
            ft.replace(layoutId,fragment,tag)

        }


            ft.addToBackStack(tag)
            .commit()

        // onBackpressed in activity as checks for any fragment and calls popimmediate
        fm.addOnBackStackChangedListener {
            val newCount = fm.backStackEntryCount
            if(newCount>1)
            {
                // remove toolbar

            }
            if (newBackStackLen!=newCount)
            {
                fm.removeOnBackStackChangedListener{}
                if (newBackStackLen>newCount)
                {
                    // back is pressed
                }
            }
        }
    }

    fun performDialogTransaction(fm : FragmentManager ,  tag : String ,  fragment : DialogFragment )
    {
        fragment.show(fm,tag)
    }

    fun removePreviousTransaction( fm : FragmentManager)
    {
        fm.popBackStack()
    }


}