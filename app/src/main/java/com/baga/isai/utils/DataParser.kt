package com.baga.isai.utils

import android.database.Cursor
import com.baga.isai.models.Album

object DataParser
{
    fun loadAlbumData(cursor: Cursor?) : ArrayList<Album>
    {
        val list : ArrayList<Album>  = ArrayList()
        if (cursor!=null && cursor.count>0)
        {
            cursor.moveToFirst()
            do {
                //"_id", "album", "artist", "artist_id", "numsongs", "minyear", path, album id
                //cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getLong(3), cursor.getInt(4), cursor.getInt(5))

                val id =cursor.getLong(0)
                val album =cursor.getString(1)
                val artist =cursor.getString(2)
                val artist_id =cursor.getLong(3)
                val numsongs =cursor.getInt(4)
                val minyear =cursor.getInt(5)
                val path = cursor.getString(6)
                val albumId = cursor.getString(7)

                list.add(Album( id, album,artist,artist_id,numsongs,minyear,path,albumId))

            }while (cursor.moveToNext())
        }
        return list
    }

}