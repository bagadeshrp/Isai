package com.baga.isai.utils

import android.graphics.Color
import com.baga.isai.datapacks.SharedPref
import java.util.*

class ThemeUtil : Observable()
{


    var currentTextColor : Int =  Color.WHITE
    var currentBackgroundColor : Int =  Color.BLACK
    var CURRENT_THEME_NAME : String = defaultThemeName


    companion object
    {
        val defaultThemeName = "bright"
        val blackThemeName = "blacky"


        var currentTextColor : Int = Color.WHITE
        var currentBackgroundColor : Int = Color.WHITE

        var CURRENT_THEME_NAME : String = defaultThemeName


        var instance : ThemeUtil ? = null

        fun newInstance() : ThemeUtil
        {
            if (instance==null)
            {
                instance = ThemeUtil()
            }
            return instance!!
        }


        fun initialize()
        {
            CURRENT_THEME_NAME = SharedPref.getString(SharedPref.SELECTED_THEME,defaultThemeName);

            when (CURRENT_THEME_NAME)
            {
                defaultThemeName -> {
                    currentTextColor = Color.BLACK
                    currentBackgroundColor = Color.WHITE
                }

                blackThemeName -> {
                    currentTextColor = Color.WHITE
                    currentBackgroundColor = Color.BLACK
                }

            }


        }
        fun enableDarkMode()
        {
            CURRENT_THEME_NAME = blackThemeName
            currentTextColor = Color.WHITE
            currentBackgroundColor = Color.BLACK


        }
        fun disableDarkMode()
        {
            CURRENT_THEME_NAME = defaultThemeName
            currentTextColor = Color.BLACK
            currentBackgroundColor = Color.WHITE


        }
    }


    fun enableDarkMode()
    {
        CURRENT_THEME_NAME = blackThemeName
        currentTextColor = Color.WHITE
        currentBackgroundColor = Color.BLACK

        SharedPref.put(SharedPref.SELECTED_THEME, blackThemeName)

        setChanged()
        this.notifyObservers()


    }
    fun disableDarkMode()
    {
        CURRENT_THEME_NAME = defaultThemeName
        currentTextColor = Color.BLACK
        currentBackgroundColor = Color.WHITE

        SharedPref.put(SharedPref.SELECTED_THEME, defaultThemeName)
        setChanged()
        this.notifyObservers()


    }


}