package com.baga.isai.utils

enum class SongMetadata {
    ALBUM,
    SONG
}