package com.baga.isai

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.baga.isai.callbacks.MainActivityCallbacks
import com.baga.isai.customcomp.PreCachingLayoutManager
import com.baga.isai.fragments.AlbumFragment
import com.baga.isai.fragments.MoreBottomSheetFragment
import com.baga.isai.navigation.NavigationRVAdapter
import com.baga.isai.presenters.InitialSetupPresenter
import com.baga.isai.showmusic.MusicListViewFragment
import com.baga.isai.utils.GeneralUtils.checkStoragePermission
import com.baga.isai.utils.TransactionUtils.performTransaction
import com.baga.isai.utils.TransactionUtils.performDialogTransaction
import com.baga.isai.utils.TransactionUtils.removePreviousTransaction
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.music_option_list.*
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.baga.isai.Base.BaseActivity
import com.baga.isai.callbacks.MusicPlayerCallbacks
import com.baga.isai.callbacks.ServiceSongCallbacks
import com.baga.isai.datapacks.SharedPref
import com.baga.isai.fragments.PlayListFragment
import com.baga.isai.fragments.SearchFragment
import com.baga.isai.models.Song
import com.baga.isai.services.MusicPlayer
import com.baga.isai.services.MusicServices
import com.baga.isai.singlemusicview.DetailsViewType
import com.baga.isai.singlemusicview.MusicPlayingFragment
import com.baga.isai.utils.*
import kotlinx.android.synthetic.main.toolbar.*


interface Service_reg
{
    fun registerMusicService()
}

/**
 *
 *
 */

class MainActivity : BaseActivity() , Service_reg
, MainActivityCallbacks,
        MusicPlayerCallbacks ,
        ServiceSongCallbacks
{

    /**
     * @see [setupBottomNavigation] to change when bottom bar is clicked
     * @see[onLiveMusicBarClicked] when live music bar is clicked
     * @see [detailsPageOpened]
     *
     */

    val MUSIC_LIST_FRAG = "Music list fragment"
    val PLAYLIST_FRAG = "PLAYLIST_FRAGMENT"
    val ALBUM_LIST_FRAG = "Albumfragment"
    val SEARCH_FRAGMENT = "SearchFragment"
    val BOTTOM_SHEET = "bottomsheet"
    val MUSIC_PLAY_FRAG = "Music play fragment"


    var toolbar : Toolbar? =null

    var musicFragment : MusicListViewFragment? = null;
    var albumFragment : AlbumFragment ? = null
    var playlistFragment : PlayListFragment? = null
    var searchFragment : SearchFragment? = null
    var detailsViewFragment : MusicPlayingFragment? = null
    var currentSelectedId : Int ? =null

    val STORAGE_PERMISSION_CODE = 922

    val theme :  ThemeUtil = ThemeUtil.newInstance()



    lateinit var musicPlayer: MusicPlayer
    var songObject : Song ? = null

    var isBindingStarted = false

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        playButton.setOnClickListener {
            onPlayPauseButtonClicked(it)
        }
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        if (!checkStoragePermission(this@MainActivity))
        {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),STORAGE_PERMISSION_CODE)
        }
        musicFragment = MusicListViewFragment.instance()
        performTransaction(supportFragmentManager,MUSIC_LIST_FRAG,musicFragment!!)
        setupNavigationView()
        updateYourTheme()
        setListener()

        musicPlayer = MusicPlayer.getSingleton()
        musicPlayer.listOfCallback.add(this)
    }
    // initers
    fun initToolbar(toolbar: Toolbar, title : String)
    {
        this.toolbar = toolbar
        setSupportActionBar(this.toolbar)

    }
    private fun toolbarInitializer()
    {
        val searchBar= main_toolbar.findViewById<EditText>(R.id.toolbarEditText)
        searchBar.addTextChangedListener( object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                searchFragment?.search(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        }  )
    }
    fun setupNavigationView()
    {
        setupBottomNavigation()
        //setupNavigationRecyclerView()
    }

    private fun setupBottomNavigation()
    {
        mainBottomNavigationBar.selectedItemId = R.id.menuSongs
        mainBottomNavigationBar.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        mainBottomNavigationBar.itemTextColor = ColorStateList.valueOf(ThemeUtil.currentTextColor)
        mainBottomNavigationBar.setItemIconTintList(ColorStateList.valueOf(ThemeUtil.currentTextColor))
        currentSelectedId = R.id.menuSongs
        mainBottomNavigationBar.setOnNavigationItemSelectedListener{
                item: MenuItem ->

            if ( item.itemId==R.id.more )
            {
                performDialogTransaction(supportFragmentManager,BOTTOM_SHEET, MoreBottomSheetFragment())
            }
            else
            {
                if (item.itemId!=currentSelectedId)
                {
                    currentSelectedId = item.itemId
                    setBottomBarAndLiveMusicViewHideOrShow(item.itemId)
                    when (item.itemId)
                    {
                        R.id.menuAlbum  -> {
                            removePreviousTransaction(supportFragmentManager)
                            albumFragment = albumFragment?:AlbumFragment()
                            performTransaction(supportFragmentManager,ALBUM_LIST_FRAG,albumFragment!!)
                        }
                        R.id.menuSongs  -> {
                            removePreviousTransaction(supportFragmentManager)
                            musicFragment = musicFragment?: MusicListViewFragment.instance()
                            performTransaction(supportFragmentManager,MUSIC_LIST_FRAG,musicFragment!!)
                        }
                        R.id.menuPlaylist -> {
                            removePreviousTransaction(supportFragmentManager)
                            playlistFragment = playlistFragment?: PlayListFragment.newInstance()
                            performTransaction(supportFragmentManager,PLAYLIST_FRAG,playlistFragment!!)

                        }
                        R.id.searchSongs -> {
                            removePreviousTransaction(supportFragmentManager)
                            searchFragment= searchFragment?: SearchFragment.getNewInstance()
                            performTransaction(supportFragmentManager,SEARCH_FRAGMENT,searchFragment!!)

                        }
                        R.id.more -> {
                            performDialogTransaction(supportFragmentManager,BOTTOM_SHEET, MoreBottomSheetFragment())
                        }

                    }
                }
            }



            true
        }
    }
    private fun setupNavigationRecyclerView()
    {
        val data = InitialSetupPresenter.getNavigationViewItems()
        val navigationAdapter = NavigationRVAdapter(this@MainActivity,data)
        val layout = PreCachingLayoutManager(this@MainActivity)
        layout.setExtraLayoutSpace(1)
        navigationRecyclerview.setHasFixedSize(true)
        navigationRecyclerview.layoutManager = layout
        navigationRecyclerview.adapter = navigationAdapter
    }

    private fun setBottomBarAndLiveMusicViewHideOrShow( viewId : Int){
        when (viewId){
            R.id.menuPlaylist ->{
            }
            R.id.menuAlbum ->{}
            R.id.menuSongs ->{}
            R.id.searchSongs ->{}
            R.id.more ->{}
        }
    }

    override fun onBackPressed() {
        println( " Back stack count ${supportFragmentManager.backStackEntryCount}")

        if (supportFragmentManager.backStackEntryCount>1)
        {
            // either more option or inside the song
            bottomLayout.visibility = View.VISIBLE
            updateStatusbarColor()
            showBottomNavigationBar()
            detailsPageClosed()
            removePreviousTransaction(supportFragmentManager)
        }
        else if (supportFragmentManager.backStackEntryCount==1)
        {
            // only current menu's
            when(currentSelectedId)
            {
                R.id.menuAlbum,R.id.menuSongs,R.id.menuPlaylist,R.id.searchSongs  -> finish() //R.id.menuArtist
                R.id.more -> {
                    super.onBackPressed()
                }
                else -> {
                    if (supportFragmentManager.backStackEntryCount==1)
                    {
                        finish()
                    }
                    else
                    {
                        super.onBackPressed()
                    }
                }

            }
        }
        else
        {
            super.onBackPressed()
        }





    }

    override fun registerMusicService(  ) {

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        musicFragment?.refresh()
    }

    override fun showBottomNavigationBar() {
        mainBottomNavigationBar.visibility = View.VISIBLE
    }

    override fun hideBottomNavigationBar() {
        mainBottomNavigationBar.visibility = View.GONE
    }

    fun updateStatusBarColor( color : Int)
    {
        if (color == Color.WHITE)
        {
            val window = window
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            window.statusBarColor = color
        }
        else
        {
            updateStatusbarColor()
        }

    }
    fun updateTheme()
    {
        mainBottomNavigationBar.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        mainBottomNavigationBar.itemTextColor = ColorStateList.valueOf(ThemeUtil.currentTextColor)
        mainBottomNavigationBar.setItemIconTintList(ColorStateList.valueOf(ThemeUtil.currentTextColor))
    }
    private fun updateStatusbarColor()
    {
        val window = window
        window.apply {
            if(ThemeUtil.currentTextColor == Color.WHITE)
            {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_IMMERSIVE

            }
            else
            {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

            }
            statusBarColor = ThemeUtil.currentBackgroundColor
        }

    }

    private fun setLiveMusicBarDetails (){
        songName.text = songObject?.title
        playButton.setImageResource(R.drawable.ic_pauseicon2)
    }

    override fun detailsPageOpened( fragment: MusicPlayingFragment ) {
        detailsViewFragment = fragment
        playSong(fragment.song)
        if (!isBindingStarted){
            bindMusicService()
            isBindingStarted = true;
        }
        setLiveMusicBarDetails()


//        musicFragment?.rootView?.postDelayed(Runnable {
//            musicFragment?.rootView?.visibility = View.INVISIBLE
//        },500)
//
    }

    override fun detailsPageClosed() {
            musicFragment?.rootView?.visibility = View.VISIBLE
    }

    override fun updateYourTheme() {
        updateTheme()
        updateStatusbarColor()
        updateToolbarTheme()
    }

    fun updateToolbarTheme()
    {
        if(ThemeUtil.currentBackgroundColor == Color.WHITE)
        {
            toolbarCardView.setCardBackgroundColor(ThemeUtil.currentBackgroundColor)

        }
        else
        {
            toolbarCardView.setCardBackgroundColor(getColor(R.color.greyForToolbar))

        }
        main_toolbar.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        toolbarEditText.setHintTextColor(ThemeUtil.currentTextColor)

    }

    override fun addObservers() {
       theme.addObserver(this)
    }

    private fun setListener()
    {
        toolbarInitializer()
//        playButton.setOnClickListener {
//            if(songCounter.isPlaying())
//            {
//                ComponentUtils.stopMusicServices(this@MainActivity)
//                paused()
//                songCounter.pause()
//            }
//            else
//            {
//                if (songCounter.getCurrentSong()!=null)
//                {
//                    ComponentUtils.startMusic(this@MainActivity,songCounter.getCurrentSong()!!)
//                    resume()
//                    songCounter.resume()
//                }
//            }
//        }

    }

    private fun bindMusicService(){
        musicPlayer = MusicPlayer.getSingleton()
        val intent: Intent = Intent(this, MusicServices::class.java).setAction(MusicServices.INIT)
        intent.putExtra(AppController.SONG_PARCEL,songObject)
        musicPlayer.bindService(intent)
        musicPlayer.listOfCallback.add(this)
    }


    private fun hideToolbar()
    {
        main_toolbar.visibility = View.INVISIBLE
    }
    private fun showToolbar()
    {



        main_toolbar.visibility = View.VISIBLE
    }

    fun onLiveMusicBarClicked ( view : View){
        if(songObject!=null){
            openDetailsViewFragment()
        }
    }

    fun onPlayPauseButtonClicked (view : View){
        val currentSong =songObject
        if ( musicPlayer.musicService != null && musicPlayer.isMusicPlaying() ){
            musicPlayer.pauseMusic()
            playButton.setImageResource(R.drawable.ic_playbutton2)
        }
        else
        {
            if (musicPlayer.musicService!=null && currentSong != null){
                musicPlayer.startMusic(song = currentSong)
                playButton.setImageResource(R.drawable.ic_pauseicon2)

            }
        }
    }


    override fun serviceConnected() {
        musicPlayer.musicService?.listOfSongCallbacks?.add(this)
    }

    override fun serviceDestroyed() {

    }

    override fun songStartedPlaying(song : Song) {
        songObject = song
        songName.text = song.title
        playButton.setImageResource(R.drawable.ic_pauseicon2)
        sliderMusic.setTimer()
    }

    override fun songPaused() {
    }

    override fun songResume() {
    }

    override fun songCompleted() {
        playButton.setImageResource(R.drawable.ic_playbutton2)
        checkAndPlayNextSong()
    }

    fun openDetailsViewFragment()
    {
        val frag = detailsViewFragment
        if (frag!=null){
            bottomLayout.visibility = View.GONE
            hideBottomNavigationBar()
            frag.fragmentOpeningType = DetailsViewType.CONTINUE
            performTransaction(supportFragmentManager, MUSIC_PLAY_FRAG, frag,null,null,R.id.frameForInnerFragment)
        }

    }
    fun checkAndPlayNextSong(){
        val cSong =songObject
        if (SharedPref.getBoolean(SharedPref.Keys.PLAY_NEXT_SONG,true) && cSong!=null){
            val nextSong = SongQueues.getSingleton().getNextSong(cSong)
            playSong(nextSong)
        }
    }


    // Song play and UI related
    fun playSong(song : Song){
        musicPlayer.startMusic(song = song)
        songObject = song
    }

}
