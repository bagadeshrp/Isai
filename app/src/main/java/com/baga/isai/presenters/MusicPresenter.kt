package com.baga.isai.presenters

import android.database.Cursor
import com.baga.isai.database.DatabaseUtil
import com.baga.isai.datapacks.Converters
import com.baga.isai.datapacks.GeneralData
import com.baga.isai.models.Album
import com.baga.isai.models.Musicoption
import com.baga.isai.models.Song
import com.baga.isai.parsers.MusicCursorParser
import com.baga.isai.utils.DataParser.loadAlbumData

object MusicPresenter
{
    fun getMusicThreeDotOptions() : ArrayList<Musicoption>
    {
        return GeneralData.getOptionData()
    }
    fun getAlbumList() : ArrayList<Album>
    {
        val cursor =  DatabaseUtil.getAlbumFiles()
        return  loadAlbumData(cursor)
    }

    fun getSongsFromAlbum(album : String) : ArrayList<Song>
    {
        return Converters.getSongsFromAlbum(album)
    }

    /**
     *  get the songs in default order
     */
    fun getSongsList() : ArrayList<Song>
    {
        val cursor  =  DatabaseUtil.getMusicFiles()
        return MusicCursorParser.parse(cursor)
    }
}