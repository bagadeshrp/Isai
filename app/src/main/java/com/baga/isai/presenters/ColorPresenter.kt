package com.baga.isai.presenters

import com.baga.isai.datapacks.ColorDatas.randomColor

object ColorPresenter
{
    fun getColorNavigationColor () : Int
    {
        return randomColor
    }
}