package com.baga.isai.presenters

import com.baga.isai.database.DatabaseUtil
import com.baga.isai.models.Playlist
import com.baga.isai.parsers.MusicCursorParser

object DatabasePresenter
{
    fun getPlayLists() : ArrayList<Playlist>
    {
        val cursor = DatabaseUtil.getPlayListCursor()
        return MusicCursorParser.parsePlaylist(cursor)
    }

    fun addPlayList( name : String)
    {
        DatabaseUtil.addNewPlayList(name)
    }
}
