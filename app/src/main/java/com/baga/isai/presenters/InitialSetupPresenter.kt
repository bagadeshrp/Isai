package com.baga.isai.presenters

import com.baga.isai.datapacks.InitialSetupDatas
import com.baga.isai.navigation.NavigationItemModel

object InitialSetupPresenter
{


    fun getNavigationViewItems() : ArrayList<NavigationItemModel>
    {
        return InitialSetupDatas.getNavigationViewItems()
    }
    fun getNavigationViewStaticItem() : ArrayList<NavigationItemModel>
    {
        return InitialSetupDatas.getNavigationViewStaticItems()
    }




}