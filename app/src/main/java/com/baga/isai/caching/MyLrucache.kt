package com.baga.isai.caching

import android.app.ActivityManager
import android.content.Context
import android.graphics.Bitmap
import android.util.LruCache
import com.baga.isai.AppController


class MyLrucache
{
    companion object
    {
        private val instance  = MyLrucache()

        @Synchronized
        fun getInstance() : MyLrucache
        {
            return instance
        }


    }



    var lrucache: LruCache<String,Bitmap>

    init {
        // calculate cachesize
        val am= (AppController.getAppContext().getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager).memoryClass
        var cacheSize : Int
        if (am<48)
        {
            cacheSize = (am/4) *1024* 1024
        }
        else{
            cacheSize = 1024 * 1024*16
        }
        lrucache = LruCache(cacheSize)

    }


    // get bitmap
    fun get(key : String) : Bitmap ?
    {
        return lrucache.get(key)
    }

    // set bitmap to cache
    fun set ( key : String , bitmap: Bitmap )
    {
        lrucache.put(key,bitmap)
    }

}