package com.baga.isai.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.SeekBar
import com.baga.isai.R
import com.baga.isai.services.MusicPlayer
import com.baga.isai.utils.GeneralUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class CustomSliderMusic (val mContext : Context, val attrs:AttributeSet)

    : SeekBar(mContext,attrs)
{
    val paint = Paint()
    var current = 0F
    private var timer : Timer ? = null
    var each = right / 100;
    init {
        paint.apply {
            isAntiAlias = true
            style = Paint.Style.FILL_AND_STROKE
            color = mContext.getColor(R.color.greyForToolbar)
            setWillNotDraw(false)
        }
    }
    override fun onDraw(canvas: Canvas?) {
        paint.color = mContext.getColor(R.color.greyForToolbar)
        canvas?.drawRect(0f,0f,right.toFloat(),bottom.toFloat(),paint)
        paint.color = mContext.getColor(R.color.greenLight1)
        println("Current value  = ${current} ")
        canvas?.drawRect(0f,0f ,current, bottom.toFloat(),paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
    }


    fun updatePercentage ( percent : Float )
    {
        invalidate()
    }


    fun setTimer(){
        current = 0f
        invalidate()
        each = right / 100;
        timer?.cancel()
        timer?.purge()
        timer = Timer()
        var count = 0
        var player = MusicPlayer.getSingleton().musicService?.mediaPlayer
        var endTime = player?.duration
        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if (player==null){
                    player = MusicPlayer.getSingleton().musicService?.mediaPlayer
                    endTime = player?.duration
                }
                invalidate()
                updatePercentage(9f)
                GlobalScope.launch (Dispatchers.Main) {
                    println(" task thread ${Thread.currentThread().id}  count ${count}")
                    if ( player!=null && player?.isPlaying ==true ) {
                        val pos = player?.currentPosition
                        if (pos!=null && endTime!=null){
                            val per = pos /  (endTime ?: 1 ).toFloat()
                            current = ( each * (per * 100) ).toFloat()
                            println("Current value kjdvbkv = ${current}")
                            postInvalidate()

                            invalidateOutline()
                        }

                    }
                    else{
                        count ++ ;
                        if(count>5){
                            timer!!.cancel()
                            timer!!.purge()
                        }
                    }
                }

                println("Current value running ")
            }
        }, 0, 1000)
    }


}