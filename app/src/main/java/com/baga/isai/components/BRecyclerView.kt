package com.baga.isai.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.customcomp.BKRecyclerview

class BRecyclerView(context: Context, attributeSet: AttributeSet) : RecyclerView(context,attributeSet) {

    private var emptyView : View ? =null

    val adapterDataObserver = object : AdapterDataObserver(){
        override fun onChanged() {
            val adapter = adapter
            if (adapter!=null && emptyView !=null){
                if (adapter.itemCount == 0){
                    emptyView!!.visibility = View.VISIBLE
                    this@BRecyclerView.visibility = GONE
                }
                else
                {
                    emptyView!!.visibility = View.GONE
                    this@BRecyclerView.visibility = View.VISIBLE
                }
            }
        }
    }
    fun setEmptyView ( view : View){
        this.emptyView = view
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        if (adapter!=null){
            adapter.registerAdapterDataObserver(adapterDataObserver)
        }
        adapterDataObserver.onChanged()
    }

}