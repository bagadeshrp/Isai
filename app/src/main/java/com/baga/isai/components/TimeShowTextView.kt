package com.baga.isai.components

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.widget.TextView
import com.baga.isai.utils.GeneralUtils
import android.R.string.cancel
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import com.baga.isai.services.MusicPlayer
import java.util.*
import kotlin.concurrent.thread
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


enum class STATE
{
    PAUSE,PLAY,INIT,RESUME
}

class TimeShowTextView : TextView
{

    private  var millsFinished : Long = 0
    private var maxTimeInMs : Long = 0
    private var countDownTimer : CountDownTimer ? = null
    private var state : STATE = STATE.INIT
    private var mFinishListner : Result ? = null
    private var timer : Timer ? = null

    constructor(mContext : Context) : super(mContext)
    {

    }
    constructor(mContext : Context, attributeSet: AttributeSet) : super(mContext,attributeSet)
    {

    }


    public fun reset()
    {
        maxTimeInMs = 0
        millsFinished= 0
        updateUi(0)
        countDownTimer?.cancel()
        countDownTimer = null
        state = STATE.INIT
    }

    private fun updateUi( mills : Long )
    {
        var time = mills
        if (time!=0L){
            time = maxTimeInMs - time
        }
        text = GeneralUtils.getTimeInMMSS(time)

    }



    interface Result
    {
        fun finished()
    }

    fun setOnFinishListener( result : Result)
    {
        mFinishListner = result
    }


    fun update( currentTime : Long)
    {
        text = GeneralUtils.getTimeInMMSS(currentTime)

    }


    fun setTimer()
    {
        timer?.cancel()
        timer?.purge()
        timer = Timer()
        var count = 0
        var player = MusicPlayer.getSingleton().musicService?.mediaPlayer
        timer!!.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if (player==null){
                    player = MusicPlayer.getSingleton().musicService?.mediaPlayer
                }
                GlobalScope.launch (Dispatchers.Main) {
                    println(" task thread ${Thread.currentThread().id}  count ${count}")
                    if ( player!=null && player!!.isPlaying ) {
                        text = GeneralUtils.getTimeInMMSS(player!!.currentPosition.toLong())
                    }
                    else{
                        count ++ ;
                        if(count>5){
                            timer!!.cancel()
                            timer!!.purge()
                        }
                    }
                }
            }
        }, 0, 1000)

    }
}