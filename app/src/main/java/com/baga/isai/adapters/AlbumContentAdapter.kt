package com.baga.isai.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.R
import com.baga.isai.models.Song
import kotlinx.android.synthetic.main.album_item_layout.view.*

class AlbumContentAdapter (val mContext : Context , val dataList : ArrayList<Song>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view  = LayoutInflater.from(mContext).inflate(R.layout.album_item_layout,parent,false)
        val holder = AlbumContentViewHolder(view)
        holder.optionsView.setOnClickListener{
            onOptionsClick(holder)
        }
        return holder
    }

    override fun getItemCount(): Int {
       return  dataList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val  holder = holder as AlbumContentViewHolder
        val song = dataList.get(position)
        holder.index.text = (position+1).toString()
        holder.songName.text = song.title
        holder.duration.text = song.duration
    }

    class AlbumContentViewHolder (itemview : View) : RecyclerView.ViewHolder(itemview)
    {
        val index  = itemview.findViewById<TextView>(R.id.index)
        val songName  = itemview.findViewById<TextView>(R.id.songName)
        val duration  = itemview.findViewById<TextView>(R.id.songDuration)
        val optionsView  = itemview.findViewById<ImageView>(R.id.songOption)
    }

    fun onOptionsClick( holder : AlbumContentViewHolder)
    {

    }
}