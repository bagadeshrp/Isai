package com.baga.isai.adapters

import android.content.ComponentCallbacks
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.R
import com.baga.isai.caching.MyLrucache
import com.baga.isai.models.Song
import com.baga.isai.showmusic.AsyncDrawable
import com.baga.isai.showmusic.AsyncImageDecorder
import com.baga.isai.showmusic.ImageTaskUtils
import com.baga.isai.showmusic.MainRVAdapter
import com.baga.isai.utils.SongMetadata
import com.baga.isai.utils.ThemeUtil
import org.w3c.dom.Text
import java.lang.Exception

class SearchAdapter (val context: Context , val data : ArrayList<Song> ,val callbacks: SearchCallbacks ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() , Filterable{


    var filteredResult : ArrayList<Song> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        if (viewType == -1){
            val view = LayoutInflater.from(context).inflate(R.layout.no_content,parent,false)
            val holder = SearchNoContent(view)
            holder.title.text = "No content"
            return holder
        }
        else
        {
            val view = LayoutInflater.from(context).inflate(R.layout.single_music_layout,parent,false)
            val holder = SearchViewHolder(view)
            holder.outerArtView.visibility = View.GONE
            holder.ptimeView.visibility = View.GONE
            return holder
        }


    }

    override fun getItemCount(): Int {
        if (filteredResult.size == 0){
            callbacks.showEmptyContent()
        }
        else{
            callbacks.hideEmptyContent()
        }
        return filteredResult.size
    }

    override fun getItemViewType(position: Int): Int {
        if (filteredResult.size == 0){
            callbacks.showEmptyContent()
        }
        else{
            callbacks.hideEmptyContent()
        }
        return super.getItemViewType(position)
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if(filteredResult.size == 0){

            // show no content
            callbacks.showEmptyContent()

        }
        else
        {
            callbacks.hideEmptyContent()

            val data = filteredResult.get(position)

            val mHolder = holder as SearchAdapter.SearchViewHolder
            try {
                mHolder.titleView.text = data.title
                mHolder.artistView.text = data.artist

                // theme
                mHolder.titleView.setTextColor(ThemeUtil.currentTextColor)
                mHolder.artistView.setTextColor(ThemeUtil.currentTextColor)

                mHolder.rootLayout.setBackgroundColor(ThemeUtil.currentBackgroundColor)
                mHolder.optionIcon.setColorFilter(ThemeUtil.currentTextColor)

            }
            catch (e : Exception){
                e.printStackTrace()
            }
        }

    }

    inner class SearchViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView){
        val titleView  = itemView.findViewById<TextView>(R.id.singleMLtextView)
        val artistView = itemView.findViewById<TextView>(R.id.singleMLartist)
        val ptimeView = itemView.findViewById<TextView>(R.id.singleMLplaytime)
        val artView = itemView.findViewById<ImageView>(R.id.singleMLart)
        val outerArtView = itemView.findViewById<CardView>(R.id.singleMLcardart)
        val optionIcon =  itemView.findViewById<ImageView>(R.id.singleMLoption)
        val cLayout =  itemView.findViewById<ConstraintLayout>(R.id.singleMLSongLayout)
        val rootLayout =  itemView.findViewById<LinearLayout>(R.id.singleMLRoot)
    }

    inner class SearchNoContent(itemView : View) : RecyclerView.ViewHolder(itemView){

        val image = itemView.findViewById<ImageView>(R.id.image)
        val title = itemView.findViewById<TextView>(R.id.title)
        val root = itemView.findViewById<LinearLayout>(R.id.root)
    }

    override fun getFilter(): Filter {
        return MySearchFilter()
    }

    inner class MySearchFilter : Filter(){
        override fun performFiltering(constraint: CharSequence): FilterResults {


            if (constraint.isEmpty()){
                filteredResult = data
            }
            else{
                val searchWord = constraint.toString()
                val tempResult : ArrayList<Song> = ArrayList()
                for ( song in data ){
                    if ( song.title.contains(searchWord,true)  ){
                        tempResult.add(song)
                    }
                }

                filteredResult = tempResult
            }
            val filteredResults = FilterResults()
            filteredResults.values = filteredResult

            return filteredResults

        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            filteredResult = results.values as ArrayList<Song>
            notifyDataSetChanged()

        }
    }

    interface SearchCallbacks{
        fun showEmptyContent()
        fun hideEmptyContent()
    }
}