package com.baga.isai.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.R
import com.baga.isai.models.Playlist
import com.baga.isai.utils.ThemeUtil
import kotlinx.android.synthetic.main.playlist_view.view.*

class PlayListRecyclerViewAdapter (val mContext : Context , val listOfData : ArrayList<Playlist>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.playlist_view,parent,false)
        val holder = PlaylistHolder(view)
        return holder
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = listOfData[position]
        val holder = holder as PlaylistHolder



        holder.apply {

            playlistName.text = item.playlistTitle
            playlistCount.text = " ${item.songCount} Songs"
            playlistDuration.text = item.duration

            //        theme
            playlistName.setTextColor(ThemeUtil.currentTextColor)
            playlistDuration.setTextColor(ThemeUtil.currentTextColor)
            playlistCount.setTextColor(ThemeUtil.currentTextColor)
            playlistIcon.setColorFilter(ThemeUtil.currentTextColor)
            playlistMenuIcon.setColorFilter(ThemeUtil.currentTextColor)
            rootView.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        }
    }

    class PlaylistHolder ( view : View) : RecyclerView.ViewHolder( view)
    {
        val playlistIcon = view.findViewById<ImageView>(R.id.playlistIcon)
        val playlistMenuIcon = view.findViewById<ImageView>(R.id.playlistMenu)
        val playlistName = view.findViewById<TextView>(R.id.playlistName)
        val playlistCount = view.findViewById<TextView>(R.id.playlistCount)
        val playlistDuration = view.findViewById<TextView>(R.id.playlistTotalDuration)
        val rootView = view.findViewById<LinearLayout>(R.id.playlistHorizontalView)
    }
}