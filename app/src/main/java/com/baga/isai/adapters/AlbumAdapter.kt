package com.baga.isai.adapters

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.AppController.Companion.ALBUM_CONTENT_FRAG
import com.baga.isai.R
import com.baga.isai.caching.MyLrucache
import com.baga.isai.fragments.AlbumContentFragment
import com.baga.isai.models.Album
import com.baga.isai.presenters.MusicPresenter
import com.baga.isai.showmusic.AsyncDrawable
import com.baga.isai.showmusic.AsyncImageDecorder
import com.baga.isai.showmusic.ImageTaskUtils
import com.baga.isai.utils.SongMetadata
import com.baga.isai.utils.TransactionUtils

class AlbumAdapter (private val mContext: Context,private val albumList : List<Album>) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

    val bitmap : Bitmap? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.album_item,parent,false)
        val holder  = Albumholder(view)
        holder.itemView.setOnClickListener{
            onItemClick(holder)
        }
        return holder
    }

    override fun getItemCount(): Int {
        return albumList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val albumItem  = albumList.get(position)
        val holder = holder as Albumholder
        holder.albumName.text = albumItem.title
        holder.albumArtist.text = albumItem.artistName

        //load bitmap
        val bitmap = MyLrucache.getInstance().get(albumItem.id.toString())
        if (bitmap!=null)
        {
            holder.albumImage.setImageBitmap(bitmap)
        }
        else if (ImageTaskUtils.cancelPotentialTasks(albumItem.id.toString(),holder.albumImage))
        {
            if (albumItem.path==null)
            {

            }
            else
            {
                val task = AsyncImageDecorder(mContext,holder.albumImage,albumItem.path,albumItem.id.toString(),SongMetadata.ALBUM)
                val asyncDrawable = AsyncDrawable(mContext.resources,bitmap,task)
                holder.albumImage.setImageDrawable(asyncDrawable)
                task.execute()
            }

        }
    }

    class Albumholder (itemView : View) : RecyclerView.ViewHolder(itemView)
    {
         val cardView : CardView = itemView.findViewById(R.id.albumCardView)
         val albumImage : ImageView = itemView.findViewById(R.id.albumGridImage)
         val albumName : TextView = itemView.findViewById(R.id.albumName)
         val albumArtist : TextView = itemView.findViewById(R.id.albumArtist)
//         val menuButton : ImageView = itemView.findViewById(R.id.albumMenu)
    }

    fun onItemClick(holder :Albumholder )
    {
        val position = holder.adapterPosition
        val album = albumList.get(position)
        val list = MusicPresenter.getSongsFromAlbum(album.albumId)
        TransactionUtils.performTransaction((mContext as AppCompatActivity).supportFragmentManager,ALBUM_CONTENT_FRAG,AlbumContentFragment.instance(list,album))
    }



}