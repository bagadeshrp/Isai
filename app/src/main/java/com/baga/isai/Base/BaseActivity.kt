package com.baga.isai.Base

import androidx.appcompat.app.AppCompatActivity
import java.util.*

abstract class BaseActivity : AppCompatActivity() ,  Observer
{

    override fun onStart() {
        super.onStart()
        addObservers()
    }

    override fun update(o: Observable?, arg: Any?) {
        updateYourTheme()
    }

    abstract fun updateYourTheme()
    abstract fun addObservers();
}