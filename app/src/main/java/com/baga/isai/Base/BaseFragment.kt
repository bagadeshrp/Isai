package com.baga.isai.Base

import android.os.Bundle
import androidx.fragment.app.Fragment
import java.util.*

abstract class BaseFragment : Fragment(), Observer
{
    override fun update(o: Observable?, arg: Any?) {

    }

    override fun onStart() {
        super.onStart()
        addObservers()
    }
    abstract fun addObservers()
}