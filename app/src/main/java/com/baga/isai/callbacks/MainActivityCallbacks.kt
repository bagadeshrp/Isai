package com.baga.isai.callbacks

import com.baga.isai.fragments.SearchFragment
import com.baga.isai.singlemusicview.MusicPlayingFragment

interface MainActivityCallbacks
{
    fun showBottomNavigationBar()
    fun hideBottomNavigationBar()

    fun detailsPageOpened( fragment: MusicPlayingFragment)
    fun detailsPageClosed()

}