package com.baga.isai.callbacks

import com.baga.isai.models.Song

interface ServiceSongCallbacks
{
    fun songStartedPlaying( song : Song )
    fun songPaused()
    fun songResume()
    fun songCompleted()
}
