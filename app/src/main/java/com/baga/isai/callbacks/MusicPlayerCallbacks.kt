package com.baga.isai.callbacks

interface MusicPlayerCallbacks
{
    fun serviceConnected()
    fun serviceDestroyed()
}
