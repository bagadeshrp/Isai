package com.baga.isai.callbacks

interface RefreshCallBack {
    fun refresh()
}