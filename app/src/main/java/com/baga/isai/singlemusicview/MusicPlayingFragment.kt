package com.baga.isai.singlemusicview


import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.graphics.ColorFilter
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.baga.isai.AppController
import com.baga.isai.AppController.Companion.IS_MUSIC_PL
import com.baga.isai.AppController.Companion.KEY_TOTAL_COUNT
import com.baga.isai.Base.BaseFragment
import com.baga.isai.MainActivity

import com.baga.isai.R
import com.baga.isai.callbacks.MainActivityCallbacks
import com.baga.isai.callbacks.MusicPlayerCallbacks
import com.baga.isai.callbacks.ServiceSongCallbacks
import com.baga.isai.components.TimeShowTextView
import com.baga.isai.database.DatabaseUtil.changeCurrentPlaying
import com.baga.isai.datapacks.SharedPref
import com.baga.isai.models.Song
import com.baga.isai.presenters.MusicPresenter.getSongsList
import com.baga.isai.services.MusicPlayer
import com.baga.isai.services.MusicServices
import com.baga.isai.services.MusicServices.Companion.ALBUM
import com.baga.isai.services.MusicServices.Companion.ARTIST
import com.baga.isai.services.MusicServices.Companion.DATA_KEY
import com.baga.isai.services.MusicServices.Companion.DATA_PATH
import com.baga.isai.services.MusicServices.Companion.NAME
import com.baga.isai.utils.ComponentUtils
import com.baga.isai.utils.SongUtils
import com.baga.isai.utils.ThemeUtil
import kotlinx.android.synthetic.main.fragment_music_playing.*
import kotlinx.android.synthetic.main.fragment_music_playing.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.coroutines.GlobalScope

enum class DetailsViewType {
    DEFAULT,CONTINUE
}

class MusicPlayingFragment : BaseFragment
,TimeShowTextView.Result
, ServiceSongCallbacks, MusicPlayerCallbacks
{
    private var rootView : View ? = null
    lateinit var song : Song
    private var totalCount : Int=0
    private var isMusicPlaying : Boolean = false
    private lateinit var songList : ArrayList<Song>
    private lateinit var mainCallBack : MainActivityCallbacks
    private lateinit var musicPlayer : MusicPlayer
    private lateinit var songUtils: SongUtils
    private var lastSongPosition : Int = 0
    var fragmentOpeningType = DetailsViewType.DEFAULT
    companion object
    {
        fun instance(song : Song, totalCount : Int ) : MusicPlayingFragment
        {
            val m = MusicPlayingFragment(song)
            val bundle = Bundle()
            bundle.putInt(KEY_TOTAL_COUNT,totalCount)
            m.arguments = bundle
            return m
        }
    }



    constructor(song: Song)
    {
        this.song = song

    }

    constructor()

    val touchListener = object  : View.OnTouchListener{
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            return false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(rootView!=null)
        {
            return rootView
        }
        rootView = inflater.inflate(R.layout.fragment_music_playing, container, false)
        rootView!!.setOnTouchListener(touchListener)
        mainCallBack = (activity as MainActivity)
        mainCallBack.detailsPageOpened(this)
        parseArguments(arguments)
        return rootView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        songList = getSongsList()
        musicPlayRunTime.setOnFinishListener(this)
        setupViewPager(view)
        setupMusicData()
        setupListeners();
        updateTheme()
        bindService()
        checkAndMaintainContinuty()
    }

    fun bindService(){
        musicPlayer = MusicPlayer.getSingleton()
        songUtils = SongUtils.getSingleton(songList)
        val intent: Intent = Intent(context, MusicServices::class.java).setAction(MusicServices.INIT)
        intent.putExtra(AppController.SONG_PARCEL,song)
        musicPlayer.bindService(intent)
        musicPlayer.listOfCallback.add(this)
    }

    fun checkAndMaintainContinuty(){
        if (fragmentOpeningType == DetailsViewType.CONTINUE){
            //song is playing in the background
            musicPlayRunTime.setTimer()
            if (musicPlayer.isMusicPlaying()){
                // pause buton
                showPauseButton()
            }
            else{
                showPlayButton()
            }
        }
    }
    // setters
    private fun parseArguments(bundle: Bundle?)
    {
        totalCount = bundle?.getInt(KEY_TOTAL_COUNT) ?: 0
    }

    private fun setupViewPager(view : View)
    {
        lastSongPosition = song.number
        val pager =  view.findViewById<ViewPager>(R.id.musicPlayViewPager)
        pager.clipToPadding =  false
        pager.pageMargin =20
        pager.setPadding(100,0,100,0)
        val adpater   = SingleMusicViewPagerImageAdapter(context!!,song,songList)
        pager.adapter = adpater
        pager.currentItem = song.number-1
        pager.addOnPageChangeListener( object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                // position will be song number -1 so be aware of that
                changeSong(position)
                lastSongPosition = position
            }
        })


    }

    private fun setupMusicData()
    {
        musicPlaySongName.text = song.title
        musicPlayArtistName.text = song.artist
        musicPlayMovieName.text = song.album
        musicPlayEndTime.text = song.duration
        musicPlayCounter.text = String.format("${song.number} of ${totalCount}")
        changeCurrentPlaying(context!!,song)
    }
    private fun setupListeners()
    {
        musicPlayPP.setOnClickListener {
            toggleCurrent()
        }

        musicPlayPrev.setOnClickListener{
            playPrevious()
        }
        musicPlayNext.setOnClickListener{
            playNextSong()
        }
    }
    private fun updateCurrentSongInfo()
    {
        setupMusicData()

    }

    /**
     * Will change song based on the position
     */
    private fun changeSong(position : Int)
    {
        val songNumber = position
        if (songNumber >0 && songNumber < songList.size)
        {
            // a valid position
            val tp = songList[songNumber]
            song = tp
            musicPlayer.startMusic(song = song)
            musicPlayRunTime.setTimer()
            updateCurrentSongInfo()
            showPauseButton()
        }
    }

    override fun addObservers() {
        ThemeUtil.newInstance().addObserver(this)
    }

    override fun update(o: Observable?, arg: Any?) {

        if (o is ThemeUtil)
        {
            if (this.isVisible)
            {
                updateTheme()

            }
        }
    }

    private fun updateTheme(){

        musicPlaySongName.setTextColor(ThemeUtil.currentTextColor)
        musicPlayArtistName.setTextColor(ThemeUtil.currentTextColor)
        musicPlayMovieName.setTextColor(ThemeUtil.currentTextColor)
        musicPlayEndTime.setTextColor(ThemeUtil.currentTextColor)
        musicPlayCounter.setTextColor(ThemeUtil.currentTextColor)
        musicPlayRunTime.setTextColor(ThemeUtil.currentTextColor)

        musicPlayViewPager.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        singleMLTop.setBackgroundColor(ThemeUtil.currentBackgroundColor)

//        icon
        musicPlayPrev.setColorFilter(ThemeUtil.currentTextColor)
        musicPlayNext.setColorFilter(ThemeUtil.currentTextColor)
        musicPlayPP.setColorFilter(ThemeUtil.currentTextColor)
        musicPlayShuffle.setColorFilter(ThemeUtil.currentTextColor)
        musicPlayEqualizer.setColorFilter(ThemeUtil.currentTextColor)
        musicPlayRepeat.setColorFilter(ThemeUtil.currentTextColor)


        ( activity as MainActivity ).updateStatusBarColor(ThemeUtil.currentBackgroundColor)

    }

    override fun finished() {
        playNextSong()
    }

    /**
     * Playes the next song
     */
    private fun playNextSong()
    {
        // play next song
        val nextSong = songUtils.getNextSong(song)
        if (nextSong != null){
            song = nextSong
            musicPlayer.startMusic(song = song)
            musicPlayViewPager.currentItem = musicPlayViewPager.currentItem+1
            showPauseButton()
            musicPlayRunTime.setTimer()
        }
    }

    private fun playPrevious()
    {
        val previousSong = songUtils.getPreviousSong(song)
        if (previousSong != null){
            song = previousSong
            musicPlayer.startMusic(song = song)
            musicPlayViewPager.currentItem = musicPlayViewPager.currentItem-1
            showPauseButton()
            musicPlayRunTime.setTimer()
        }
    }
    private fun toggleCurrent()
    {
        if (musicPlayer.isMusicPlaying()){
            musicPlayer.pauseMusic()
            showPlayButton()
            musicPlayRunTime.setTimer()

        }
        else {
            musicPlayer.startMusic(song =song )
            showPauseButton()
            musicPlayRunTime.setTimer()
        }
    }

    override fun songStartedPlaying(song : Song) {

    }

    override fun songPaused() {
    }

    override fun songResume() {
    }

    override fun songCompleted() {
        playNextSong()
    }

    override fun serviceConnected() {
        musicPlayer.musicService?.listOfSongCallbacks?.add(this)
    }

    override fun serviceDestroyed() {
    }




    // For UI related changes
    fun showPauseButton(){
        musicPlayPP?.setImageResource(R.drawable.ic_pauseicon2)
    }
    fun showPlayButton(){
        musicPlayPP?.setImageResource(R.drawable.ic_playbutton2)

    }

}
