package com.baga.isai.singlemusicview

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.PagerAdapter
import com.baga.isai.R
import com.baga.isai.caching.MyLrucache
import com.baga.isai.showmusic.AsyncDrawable
import com.baga.isai.showmusic.AsyncImageDecorder
import com.baga.isai.showmusic.ImageTaskUtils
import com.baga.isai.models.Song
import com.baga.isai.utils.SongMetadata

class SingleMusicViewPagerImageAdapter (val mContext: Context , val song : Song, val  fullSongList : ArrayList<Song>) : PagerAdapter()
{
    val NUM = 10
    val tpBitmap : Bitmap? =null


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layout = LayoutInflater.from(mContext).inflate(R.layout.music_now_image_layout,container,false)
        val card = layout.findViewById<CardView>(R.id.musicNowCardView)
        val imageView = layout.findViewById<ImageView>(R.id.musicNowSongImage)
        val bitmap = MyLrucache.getInstance().get(fullSongList[position].key)
        if( bitmap!=null )
        {
            imageView.setImageBitmap(bitmap)
        }
        else if(ImageTaskUtils.cancelPotentialTasks( fullSongList[position].path,imageView ) ) {
            val ail = AsyncImageDecorder(mContext, imageView, fullSongList[position].path, fullSongList[position].key,SongMetadata.SONG)
            val asyncDrawable = AsyncDrawable(mContext.resources, tpBitmap, ail)
            imageView.setImageDrawable(asyncDrawable)
            ail.execute()
        }
        container.addView(layout)
        return layout

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return fullSongList.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}