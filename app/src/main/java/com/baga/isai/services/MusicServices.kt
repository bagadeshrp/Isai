package com.baga.isai.services

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.os.AsyncTask
import android.os.Binder
import android.os.IBinder
import com.baga.isai.AppController
import com.baga.isai.R
import com.baga.isai.callbacks.ServiceSongCallbacks
import com.baga.isai.models.Song
import com.baga.isai.notification.NotificationPanel
import java.lang.Exception
import java.lang.ref.WeakReference

enum class MusicState
{
    INIT,PLAY,PAUSE,STOP
}

class MusicServices : Service() , MediaPlayer.OnCompletionListener,MediaPlayer.OnPreparedListener
{
    override fun onCompletion(mp: MediaPlayer) {
        sendSongCompletedEvent()
        handleStop()
        state = MusicState.STOP
    }

    override fun onPrepared(mp: MediaPlayer) {
        System.out.println("onPrepared is called")
        isPreparing = false
        mp.start()
        state = MusicState.PLAY

    }

    companion object
    {
        val INIT = "initial"
        val PLAY = "playMusic"
        val PAUSE = "pauseMusic"
        val STOP = "stopMusic"

        val DATA_PATH = "data_path"
        val DATA_KEY = "keyofsong"
        val NAME = "NameOfSong"
        val ALBUM = "AlbumOfSong"
        val ARTIST = "ArtistOfSong"
    }
    var manager : NotificationManager ? = null
    var mediaPlayer : MediaPlayer ? = null
    var path : String? = ""
    var state : MusicState = MusicState.STOP
    var songId : String ? = null
    var title : String? =""
    var album : String? =""
    var artist : String? =""
    lateinit var notificationPanel: NotificationPanel
    var commandState : String?= null
    var isPreparing : Boolean = false


    val listOfSongCallbacks : ArrayList<ServiceSongCallbacks> = ArrayList()
    var songObject : Song ? = null

    inner class MusicServiceBinder : Binder()
    {
        fun getService() : MusicServices
        {
            return this@MusicServices
        }
    }
    private val mBinder : IBinder = MusicServiceBinder()
    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }
    private val isServiceInit : Boolean = false

    override fun onCreate() {
        super.onCreate()
        notificationPanel = NotificationPanel(this);
        manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationPanel.createNotificationChannel(manager)

        mediaPlayer = MediaPlayer()
        mediaPlayer?.setOnCompletionListener(this)
        mediaPlayer?.setOnPreparedListener(this)


        System.out.println("onCreate is called")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        path = intent.getStringExtra(DATA_PATH)

        checkSong(intent)

        commandState = intent.action

        println( " commandState = ${commandState}" )

        when(intent.action)
        {
            INIT -> handleInit()
            PLAY -> {
                songObject = intent.getParcelableExtra(AppController.SONG_PARCEL)
                handlePlay()
            }
            PAUSE -> handlePause()
            STOP -> handleStop()
        }
        return START_NOT_STICKY
    }
    private fun checkSong(intent: Intent)
    {
        val newSongId  = intent.getStringExtra(DATA_KEY)
        if (songId==null && newSongId==null)
        {
            // null == null ex: new song or same song
            println("ERROR")
        }
        else if (songId==null && newSongId!=null)
        {
            //new Song
            songId = newSongId
            state = MusicState.STOP
        }
        else if(songId!=null && newSongId!=null)
        {
            // might be pause or new new song
            if (songId != newSongId)
            {
                // new new song
                songId=newSongId
                state = MusicState.STOP
            }
        }

        title = intent.getStringExtra(NAME)
        album = intent.getStringExtra(ALBUM)
        artist = intent.getStringExtra(ARTIST)
    }
    private fun handleInit()
    {
        Thread {
            if (!isServiceInit){
                initServices()
            }

        }.start()

    }
    private fun handlePause()
    {
        pauseNotification()
        state = MusicState.PAUSE
        mediaPlayer?.pause()

    }
    private fun handlePlay()
    {
        sendSongStartedEvent()
        if (state == MusicState.STOP)
        {
            // then load data source and stuff
            setupNotification()
            mediaPlayer?.reset()
            val attributes = AudioAttributes.Builder()
            attributes.setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            println("path = ${path}")
            if (path==null)
            {
                System.out.println("it should not be null")
            }
            else
            {
                if ( mediaPlayer?.isPlaying == true )
                {
                    mediaPlayer?.start()
                }
                else
                {

                    mediaPlayer?.apply {
                        isPreparing = true
                        setAudioAttributes(attributes.build())
                        setDataSource(path)
                        prepareAsync()
                    }
                }


            }

        }
        else if (state == MusicState.PAUSE)
        {
            isPreparing = false
            mediaPlayer?.start()
        }
        else if (state==MusicState.PLAY)
        {
            // already playing
        }

    }
    private fun handleStop()
    {
        state = MusicState.STOP
        this.stopSelf()

    }



    private fun initServices()
    {
        setupNotification()
    }
    private fun setupNotification()
    {

//        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
//        builder.apply {
//            setContentTitle("Music")
//            setContentText("Now playing music")
//            setPriority(PRIORITY_HIGH)
//            setSmallIcon(R.drawable.ic_equalizericon)
//        }
//        val manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        manager.notify(101,builder.build())

        ImageDecoder(this,path,"").execute()

    }
    private fun pauseNotification()
    {
        //createNotificationChannel()

        ImageDecoder(this,path,"").execute()


//        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
//        builder.apply {
//            setContentTitle("Music")
//            setContentText("Music now paused")
//            setPriority(PRIORITY_HIGH)
//            setSmallIcon(R.drawable.ic_equalizericon)
//        }
//        val manager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        manager.notify(AppController.NOTIFCATION_ID,builder.build())


    }
    private fun createNotificationChannel()
    {

    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()
        mediaPlayer = null
    }


    inner class ImageDecoder (mContext : Context, val path : String? , val key : String) : AsyncTask<Void,Void,Bitmap>()
    {
        private val weakReference : WeakReference<Context>  = WeakReference(mContext)

        override fun doInBackground(vararg params: Void?): Bitmap? {

            try {
                val mediaMetadataRetriever = MediaMetadataRetriever()
                println("musicservice path = ${path}")
                mediaMetadataRetriever.setDataSource(path)
                val data = mediaMetadataRetriever.embeddedPicture

                if (data!=null)
                {
                   return  BitmapFactory.decodeByteArray(data,0,data.size)
                }
                return null

            }
            catch ( e : Exception)
            {
                e.printStackTrace()
            }

            return null

        }

        override fun onPostExecute(result: Bitmap?) {
            if (result!=null)
            {
                println("musicservice path = bitmap is present")
                val b = notificationPanel.getBuild("","")

//                notificationPanel.remoteView.setTextViewText(R.id.songName,title)
//                notificationPanel.remoteView.setTextViewText(R.id.artistName,artist)



//                notificationPanel.remoteView.setTextViewText(R.id.songName,artist)

//                notificationPanel.remoteView.setImageViewBitmap(R.id.notification_icon,result)
                if (commandState == MusicServices.INIT || commandState == null )
                {

//                    notificationPanel.remoteView.setImageViewResource(R.id.playPauseButton,R.drawable.ic_pauseicon2)
                    startForeground(AppController.NOTIFCATION_ID,b.build())

                }
                else if ( commandState == MusicServices.PLAY )
                {
//                    notificationPanel.remoteView.setImageViewResource(R.id.playPauseButton,R.drawable.ic_pauseicon2)
                    startForeground(AppController.NOTIFCATION_ID,b.build())

                }
                else if ( commandState == MusicServices.STOP )
                {
                    startForeground(AppController.NOTIFCATION_ID,b.build())

                }
                else if ( commandState == MusicServices.PAUSE)
                {
//                    notificationPanel.remoteView.setImageViewResource(R.id.playPauseButton,R.drawable.ic_playbutton2)
                    startForeground(AppController.NOTIFCATION_ID,b.build())

                }
            }
        }
    }


    fun sendSongStartedEvent()
    {
        for (callback in listOfSongCallbacks){
            if (songObject!=null)
            {
                callback.songStartedPlaying(song = songObject!!)
            }
        }
    }
    fun sendSongPausedEvent()
    {
        for (callback in listOfSongCallbacks){
            callback.songPaused()
        }
    }
    fun sendSongResumedEvent()
    {
        for (callback in listOfSongCallbacks){
            callback.songResume()
        }
    }
    fun sendSongCompletedEvent()
    {
        for (callback in listOfSongCallbacks){
            callback.songCompleted()
        }
    }




}