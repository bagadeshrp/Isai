package com.baga.isai.services

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.IBinder
import com.baga.isai.AppController
import com.baga.isai.callbacks.MusicPlayerCallbacks
import com.baga.isai.models.Song
import com.baga.isai.utils.ComponentUtils

class MusicPlayer {
    companion object {
        private var sIntance : MusicPlayer ? = null

        fun getSingleton() : MusicPlayer{
            if (sIntance == null )
            {
                sIntance = MusicPlayer()
                return sIntance!!
            }
            return sIntance!!
        }

    }

    val listOfCallback : ArrayList<MusicPlayerCallbacks> = ArrayList()

    var musicService : MusicServices ? = null

    fun bindService( serviceIntent : Intent , connection: ServiceConnection = serviceConnection , flag : Int = Context.BIND_AUTO_CREATE   ) {
        AppController.getAppContext().bindService(serviceIntent , connection  ,flag)
    }
    fun unBindService( connection: ServiceConnection ) {
        AppController.getAppContext().unbindService(connection)
    }

    fun startMusic(context: Context = AppController.getAppContext(), song : Song) {
        val intent: Intent = Intent(context, MusicServices::class.java).setAction(MusicServices.PLAY)
        intent.putExtra(MusicServices.DATA_PATH,song.path)
        intent.putExtra(MusicServices.DATA_KEY,song.key)
        intent.putExtra(MusicServices.NAME,song.title)
        intent.putExtra(MusicServices.ALBUM,song.album)
        intent.putExtra(MusicServices.ARTIST,song.artist)
        intent.putExtra(AppController.SONG_PARCEL, song)
        startMusicService(context, intent)
    }

    fun pauseMusic(context: Context  = AppController.getAppContext(), intent: Intent = Intent(context,MusicServices::class.java).setAction(MusicServices.PAUSE)) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        }
        else {
            context.startService(intent)
        }
    }

    private fun startMusicService(context: Context, intent: Intent = Intent(context,MusicServices::class.java).setAction(MusicServices.INIT)) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        }
        else {
            context.startService(intent)
        }
    }

    val serviceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            sendServiceDisconnectedEvents()
        }
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            if (service!=null)
            {
                val binder = service as MusicServices.MusicServiceBinder
                musicService = binder.getService()
                sendServiceStartedEvents()
            }
        }
    }

    fun getCurrentSongTitle () : String?{
        return musicService?.title
    }
    fun getCurrentSongTime(): Int ? {
        return musicService?.mediaPlayer?.currentPosition
    }
    fun isMusicPlaying() : Boolean
    {
        return musicService?.mediaPlayer?.isPlaying == true
    }
    fun isMusicPreparing() : Boolean?
    {
        return musicService?.isPreparing
    }

    fun sendServiceStartedEvents()
    {
        listOfCallback.forEach { it.serviceConnected() }
    }
    fun sendServiceDisconnectedEvents()
    {
        listOfCallback.forEach { it.serviceDestroyed() }
    }

}