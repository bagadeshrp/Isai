package com.baga.isai.fragments

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.baga.isai.AppController
import com.baga.isai.R
import com.baga.isai.utils.ThemeUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.suke.widget.SwitchButton

class MoreBottomSheetFragment : BottomSheetDialogFragment()
{

    lateinit var cardView: CardView
    lateinit var listView: ListView
    lateinit var listViewAdapter : IsaiMoreOptionsAdapter

    fun getList() : ArrayList<String>
    {
        val ar = ArrayList<String>()
        ar.add(AppController.BOTTOM_SHEET.SHARE)
        ar.add(AppController.BOTTOM_SHEET.RATE)
        ar.add(AppController.BOTTOM_SHEET.MORE)
        ar.add(AppController.BOTTOM_SHEET.LIBRARY)
        ar.add(AppController.BOTTOM_SHEET.SETTINGS)
        ar.add(AppController.BOTTOM_SHEET.DARKMODE)

        return  ar
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val context = this@MoreBottomSheetFragment.context!!

        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        // insert our layout
        val rootView = LayoutInflater.from(context).inflate(R.layout.more_bottom_menu,null)

        cardView = rootView.findViewById(R.id.cardView)
        listView = rootView.findViewById(R.id.moreListView)


        listViewAdapter = IsaiMoreOptionsAdapter(context,R.layout.more_sheet_listview_item,getList())
        listView.adapter = listViewAdapter

        // add to parent
        dialog.setContentView(rootView)

        ( rootView.parent as View ).setBackgroundColor(ContextCompat.getColor(context,android.R.color.transparent))


        // Theming
        cardView.setCardBackgroundColor(ThemeUtil.currentBackgroundColor)

    }




    inner class IsaiMoreOptionsAdapter (val mContext : Context, val layoutResource : Int, val listOfItems : ArrayList<String> ) : ArrayAdapter<String>(mContext,layoutResource,listOfItems)
    , View.OnClickListener
    {
        override fun getItem(position: Int): String? {
            return super.getItem(position)
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view  : View? = convertView
            if (view==null)
            {
                view = LayoutInflater.from(mContext)
                    .inflate(layoutResource,null)
            }

            val title = view!!.findViewById<TextView>(R.id.listText)
            val image = view.findViewById<ImageView>(R.id.listImage)
            val switch = view.findViewById<com.suke.widget.SwitchButton>(R.id.switch_button)

            val item = getItem(position)

            view.setOnClickListener(this)
            view.setTag(item)
            title.text  =  item
            image.setImageResource(getIcon(item))

            if (item == AppController.BOTTOM_SHEET.DARKMODE)
            {
                // show switch
                switch.visibility = View.VISIBLE

                switch.isChecked = (ThemeUtil.CURRENT_THEME_NAME == ThemeUtil.blackThemeName)

                switch.setOnCheckedChangeListener(SwitchButton.OnCheckedChangeListener { view, isChecked ->

                    if (isChecked)
                    {
                        //enable dark theme
                        ThemeUtil.enableDarkMode()
                        ThemeUtil.newInstance().enableDarkMode()
                    }

                    else
                    {
                        // disbale dark theme
                        ThemeUtil.disableDarkMode()
                        ThemeUtil.newInstance().disableDarkMode()

                    }
                    refreshColor()
                })
            }
            else
            {
                switch.visibility = View.GONE
            }

            // themeing

            // help : ContextCompat.getColor(mContext,ThemeUtil.currentTextColor)

            image.setColorFilter(ThemeUtil.currentTextColor)
            title.setTextColor(ThemeUtil.currentTextColor)





            return view
        }


        fun getIcon( value : String? = "") : Int
        {
            when (value)
            {
                AppController.BOTTOM_SHEET.DARKMODE -> return R.drawable.ic_darkmode
                AppController.BOTTOM_SHEET.LIBRARY -> return R.drawable.ic_playlisticon
                AppController.BOTTOM_SHEET.MORE -> return R.drawable.ic_more
                AppController.BOTTOM_SHEET.RATE -> return R.drawable.ic_rate
                AppController.BOTTOM_SHEET.SETTINGS -> return R.drawable.ic_settingsicon
                AppController.BOTTOM_SHEET.SHARE -> return R.drawable.ic_share

                "" -> return R.drawable.ic_speakericon

            }
            return R.drawable.ic_speakericon
        }

        fun refreshColor()
        {
            cardView.setCardBackgroundColor(ThemeUtil.currentBackgroundColor)
            listViewAdapter.notifyDataSetChanged()
        }

        override fun onClick(v: View?) {
            when (v?.tag)
            {
                AppController.BOTTOM_SHEET.DARKMODE -> {
                    // toggle the swich
                    val switch = v.findViewById<com.suke.widget.SwitchButton>(R.id.switch_button)
                    switch.toggle()
                }
                AppController.BOTTOM_SHEET.LIBRARY -> {

                }
                AppController.BOTTOM_SHEET.MORE -> {

                }
                AppController.BOTTOM_SHEET.RATE -> {

                }
                AppController.BOTTOM_SHEET.SETTINGS -> {

                }
                AppController.BOTTOM_SHEET.SHARE -> {

                }

                "" -> {

                }

            }
        }
    }


}