package com.baga.isai.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.baga.isai.R
import com.baga.isai.adapters.SearchAdapter
import com.baga.isai.database.DatabaseUtil
import com.baga.isai.parsers.MusicCursorParser
import kotlinx.android.synthetic.main.fragment_search.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : Fragment(), SearchAdapter.SearchCallbacks
{
    companion object{
        private var fragment : SearchFragment ? = null

        fun getNewInstance() : SearchFragment {
            if (fragment != null ){
                return fragment!!
            }
            fragment = SearchFragment()
            return fragment!!
        }
    }
    private var rootView : View ? = null
    private lateinit var mAdapter : SearchAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView != null){
            return rootView
        }
        rootView = inflater.inflate(R.layout.fragment_search,container,false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cursor  =  DatabaseUtil.getMusicFiles()
        val lm = LinearLayoutManager(context)
        mAdapter = SearchAdapter(context!!, MusicCursorParser.parse(cursor), this@SearchFragment)
        searchRecyclerview.layoutManager = lm
        searchRecyclerview.adapter = mAdapter

    }

    fun search( searchWord : String){
        mAdapter.filter.filter(searchWord)

    }

    override fun showEmptyContent() {
        noContent.findViewById<TextView>(R.id.title).text = " No Content"
        noContent.visibility = View.VISIBLE
    }

    override fun hideEmptyContent() {
        noContent.visibility = GONE

    }
}
