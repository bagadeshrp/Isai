package com.baga.isai.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.baga.isai.R
import com.baga.isai.adapters.AlbumContentAdapter
import com.baga.isai.models.Album
import com.baga.isai.models.Song
import com.baga.isai.showmusic.AsyncDrawable
import com.baga.isai.showmusic.AsyncImageDecorder
import com.baga.isai.utils.SongMetadata
import kotlinx.android.synthetic.main.fragment_album_content.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AlbumContentFragment : Fragment{

    companion object
    {
        fun instance( list: ArrayList<Song> , album : Album) : AlbumContentFragment
        {
            val a = AlbumContentFragment(list,album)
            return a
        }
    }

    private lateinit var songList : ArrayList<Song>
    private lateinit var album : Album

    constructor( list : ArrayList<Song> , album: Album) : super()
    {
        songList = list
        this.album = album
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_album_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val lm = LinearLayoutManager(context)
        val adapter = AlbumContentAdapter(context!!,songList)

        bottomRecyclerView.hasFixedSize()
        bottomRecyclerView.layoutManager = lm
        bottomRecyclerView.adapter = adapter

        loadMediaData()
    }

    private fun loadMediaData(){
        songName.text = songList.get(0).album
        songArtist.text = songList.get(0).artist

        val ai = AsyncImageDecorder(context!!,albumImage,album.path!!,album.id.toString(),SongMetadata.ALBUM)
        val ad = AsyncDrawable(resources,null,ai)
        albumImage.setImageDrawable(ad)
        ai.execute()
    }




}
