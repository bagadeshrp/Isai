package com.baga.isai.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_album.*


import com.baga.isai.R
import com.baga.isai.adapters.AlbumAdapter
import com.baga.isai.presenters.MusicPresenter

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AlbumFragment : Fragment() {

    val GRID_COUNT = 2
    lateinit var mContext : Context

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mContext = context!!
        return inflater.inflate(R.layout.fragment_album, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val grid = GridLayoutManager(mContext,GRID_COUNT)
        val adapter = AlbumAdapter ( mContext, MusicPresenter.getAlbumList() )
        albumRecyclerView.layoutManager = grid
        albumRecyclerView.adapter = adapter

    }

}
