package com.baga.isai.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.baga.isai.MainActivity

import com.baga.isai.R
import com.baga.isai.adapters.PlayListRecyclerViewAdapter
import com.baga.isai.callbacks.RefreshCallBack
import com.baga.isai.presenters.DatabasePresenter
import com.baga.isai.utils.ThemeUtil
import com.baga.isai.utils.TransactionUtils
import kotlinx.android.synthetic.main.fragment_play_list.*

class PlayListFragment : Fragment()
,RefreshCallBack
{

    lateinit var adapter : PlayListRecyclerViewAdapter
    companion object
    {
        var instance : PlayListFragment? = null

        fun newInstance() : PlayListFragment
        {
            if (instance==null)
            {
                instance = PlayListFragment()
            }
            return instance!!

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_play_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rootView.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        val layoutManager = LinearLayoutManager(context)

        val data = DatabasePresenter.getPlayLists()
        adapter = PlayListRecyclerViewAdapter(context!!,data)
        playlistRecyclerView.layoutManager = layoutManager
        playlistRecyclerView.adapter = adapter

        addPlayList.setOnClickListener {
            val fm = (activity as MainActivity).supportFragmentManager
            TransactionUtils.performDialogTransaction(fm,"New playlist",AddPlaylistBottomFragment.newInstance(this))
        }
    }

    override fun refresh() {
        adapter.notifyDataSetChanged()
    }
}
