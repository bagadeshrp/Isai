package com.baga.isai.fragments

import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.baga.isai.R
import com.baga.isai.callbacks.RefreshCallBack
import com.baga.isai.presenters.DatabasePresenter
import com.baga.isai.utils.ThemeUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textfield.TextInputEditText

class AddPlaylistBottomFragment (val callback : RefreshCallBack )  : BottomSheetDialogFragment()
{

    companion object
    {
        var instance : AddPlaylistBottomFragment ? = null

        fun newInstance( fragment: PlayListFragment ) : AddPlaylistBottomFragment
        {
            if (instance==null)
            {
                instance = AddPlaylistBottomFragment(fragment)
            }

            return instance!!
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL,R.style.DialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return super.onCreateView(inflater, container, savedInstanceState)

    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val context : Context = context!!

        val rootView = LayoutInflater.from(context).inflate(R.layout.add_playlist_bottom_menu,null)

        val cardView = rootView.findViewById<CardView>(R.id.cardView)
        val cancel = rootView.findViewById<Button>(R.id.cancel_button)
        val create = rootView.findViewById<Button>(R.id.create_button)
        val editText = rootView.findViewById<TextInputEditText>(R.id.editText)
        val newtext = rootView.findViewById<TextView>(R.id.newPlayList)



        dialog.setContentView(rootView)
        ( rootView.parent as View).setBackgroundColor(ContextCompat.getColor(context,android.R.color.transparent))
        newtext.setTextColor(ThemeUtil.currentTextColor)
        create.setTextColor(ThemeUtil.currentTextColor)
        editText.setTextColor(ThemeUtil.currentTextColor)
        cancel.setTextColor(ThemeUtil.currentTextColor)
        editText.setHintTextColor(ThemeUtil.currentBackgroundColor)

        val cd = editText.background as GradientDrawable
        cd.color = ColorStateList.valueOf(ThemeUtil.currentTextColor)


        if (ThemeUtil.currentBackgroundColor == Color.WHITE)
        {
            cardView.setCardBackgroundColor(ThemeUtil.currentBackgroundColor)

        }
        else
        {
            cardView.setCardBackgroundColor(context.getColor(R.color.greyForToolbar))

        }





        cancel.setOnClickListener {
            dialog.dismiss()
        }


        create.setOnClickListener {
            val name = editText.text.toString()
            if (TextUtils.isEmpty(name))
            {
                // validation
            }
            else
            {
                DatabasePresenter.addPlayList(name)
                callback.refresh()
                dialog.dismiss()
            }
        }

    }
}
