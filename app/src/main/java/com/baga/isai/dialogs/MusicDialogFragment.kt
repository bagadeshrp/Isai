package com.baga.isai.dialogs

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.baga.isai.R
import com.baga.isai.models.Musicoption
import com.baga.isai.models.Song
import com.baga.isai.presenters.MusicPresenter
import com.baga.isai.utils.ThemeUtil
import kotlinx.android.synthetic.main.music_3dot_options.*

class MusicDialogFragment (val song: Song ) : DialogFragment()
{
    companion object
    {
        fun instance( song : Song) : MusicDialogFragment
        {
            val m = MusicDialogFragment(song)
            return m
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        dialog?.getWindow()?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.music_3dot_options,container,false)
        val listView = v.findViewById<ListView>(R.id.optionsListView)
        val musicOptionAdapter =  MusicOptionAdapter(context!!,R.layout.option_view,MusicPresenter.getMusicThreeDotOptions())
        listView.adapter = musicOptionAdapter

        val songName = v.findViewById<TextView>(R.id.dot3song)
        val artistName = v.findViewById<TextView>(R.id.dot3artist)
        val movieName = v.findViewById<TextView>(R.id.dot3movie)
        val songCover = v.findViewById<CardView>(R.id.dot3cover)

        songName.text = song.title
        artistName.text = song.artist
        movieName.text = song.album




        //theme

        v.setBackgroundColor(ThemeUtil.currentBackgroundColor)
        songName.setTextColor(ThemeUtil.currentTextColor)
        artistName.setTextColor(ThemeUtil.currentTextColor)
        movieName.setTextColor(ThemeUtil.currentTextColor)

        listView.setBackgroundColor(ThemeUtil.currentBackgroundColor)

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
//        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
    }
//    class MusicoptionAdapter (val mContext : Context, val list : ArrayList<Musicoption> ) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
//    {
//
//        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//            val v = LayoutInflater.from(mContext).inflate(R.layout.option_view,parent,false)
//
//            val holder = OptionViewHolder(v)
//            return holder
//
//        }
//
//        override fun getItemCount(): Int {
//                return list.size
//        }
//
//        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//            val holder = holder as OptionViewHolder
//            holder.title.text = list.get(position).title
//            holder.imageView.setImageResource( list.get(position).drawable )
//        }
//
//        class OptionViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) , View.OnClickListener
//        {
//            val imageView = itemView.findViewById<ImageView>(R.id.optionViewImage)
//            val title = itemView.findViewById<TextView>(R.id.optionViewTitle)
//            val rootView = itemView.findViewById<LinearLayout>(R.id.optionView)
//
//            init {
//                rootView.setOnClickListener(this)
//            }
//
//            override fun onClick(v: View?) {
//                if (v is LinearLayout  )
//                {
//                    // root view
//                    println("onClick of linear layout")
//                }
//            }
//        }
//    }

    inner class MusicOptionAdapter (val mContext : Context, val resourceId: Int ,val list : ArrayList<Musicoption>) : ArrayAdapter<Musicoption>(mContext,resourceId, list)
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            var view = convertView;

            if (view==null)
            {
                view = LayoutInflater.from(mContext).inflate(resourceId,null)

            }
            val item = list[position]

            val title = view!!.findViewById<TextView>(R.id.optionViewTitle)
            val image = view.findViewById<ImageView>(R.id.optionViewImage)


            title.text = item.title
            title.setTextColor(ThemeUtil.currentTextColor)
            image.setImageResource(item.drawable)
            image.setColorFilter(ThemeUtil.currentTextColor)
            view.setBackgroundColor(ThemeUtil.currentBackgroundColor)

            return view

        }
    }
}
