package com.baga.isai.showmusic

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.baga.isai.MainActivity
import com.baga.isai.R
import com.baga.isai.caching.MyLrucache
import com.baga.isai.callbacks.MainActivityCallbacks
import com.baga.isai.dialogs.MusicDialogFragment
import com.baga.isai.models.Song
import com.baga.isai.singlemusicview.MusicPlayingFragment
import com.baga.isai.utils.SongMetadata
import com.baga.isai.utils.SongQueues
import com.baga.isai.utils.ThemeUtil
import com.baga.isai.utils.TransactionUtils.performTransaction
import java.lang.Exception
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.acos

class MainRVAdapter (val mContext: Context , val musicData: ArrayList<Song> ) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
, Observer
{

    val tpBitmap : Bitmap ? =null
    val MUSIC_PLAY_FRAG = "Music play fragment"
    val fm = ( mContext as AppCompatActivity ) .supportFragmentManager
    val mainCallback : MainActivityCallbacks

    init {
        SongQueues.getSingleton().queueOfSongs = ArrayList(musicData)
        ThemeUtil.newInstance().addObserver(this)
        mainCallback = (mContext as MainActivity)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.single_music_layout,parent,false)
        val holder = MusicViewHolder(view)
        holder.cLayout.setOnClickListener {
            onSongClick(holder)
        }
        holder.optionIcon.setOnClickListener{
            onOptionClick(holder)
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val data = musicData.get(position)

            val mHolder = holder as MusicViewHolder
            try {
                mHolder.titleView.text = data.title
                mHolder.artistView.text = data.artist
                mHolder.ptimeView.text= data.duration

                // theme
                mHolder.titleView.setTextColor(ThemeUtil.currentTextColor)
                mHolder.artistView.setTextColor(ThemeUtil.currentTextColor)
                mHolder.ptimeView.setTextColor(ThemeUtil.currentTextColor)

                mHolder.rootLayout.setBackgroundColor(ThemeUtil.currentBackgroundColor)
                mHolder.optionIcon.setColorFilter(ThemeUtil.currentTextColor)

                val path = data.path
                val key = data.path

                val bitmap = MyLrucache.getInstance().get(key)
                if (bitmap!=null)
                {
                    mHolder.artView.setImageBitmap(bitmap)
                }
                else if(ImageTaskUtils.cancelPotentialTasks(path,mHolder.artView)) {
                    val ail = AsyncImageDecorder(mContext, mHolder.artView, path, key , SongMetadata.SONG)

                    val asyncDrawable = AsyncDrawable(mContext.resources, tpBitmap, ail)

                    mHolder.artView.setImageDrawable(asyncDrawable)

                    ail.execute()
                }

            }
            catch (e : Exception){
                e.printStackTrace()
            }
    }


    override fun getItemCount(): Int {
        return musicData.size
    }


    private fun onSongClick(  holder : MusicViewHolder)
    {
        // launch song fragment
        val musicPlayingFragment = getMusicDetailsFragment(holder)

        val a = ArrayList<View>()
        a.add(holder.artView)

        val b = ArrayList<String>()
        b.add( mContext.getString(R.string.MusicImageTransition) )

        mainCallback.detailsPageOpened(musicPlayingFragment)
//        mainCallback.hideBottomNavigationBar()
        //performTransaction(( mContext as AppCompatActivity).supportFragmentManager, MUSIC_PLAY_FRAG, musicPlayingFragment,a,b,R.id.frameForInnerFragment)
    }

    private fun onOptionClick(holder: MusicViewHolder)
    {
        //show options dialog
        val m = MusicDialogFragment.instance(  musicData.get( holder.adapterPosition)  )
        m.show( fm , "Option" )
    }

    inner class MusicViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val titleView  = itemView.findViewById<TextView>(R.id.singleMLtextView)
        val artistView = itemView.findViewById<TextView>(R.id.singleMLartist)
        val ptimeView = itemView.findViewById<TextView>(R.id.singleMLplaytime)
        val artView = itemView.findViewById<ImageView>(R.id.singleMLart)
        val optionIcon =  itemView.findViewById<ImageView>(R.id.singleMLoption)
        val cLayout =  itemView.findViewById<ConstraintLayout>(R.id.singleMLSongLayout)
        val rootLayout =  itemView.findViewById<LinearLayout>(R.id.singleMLRoot)
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is ThemeUtil)
        {
            notifyDataSetChanged()
        }
    }

    private fun getMusicDetailsFragment( holder: MusicViewHolder) : MusicPlayingFragment
    {
        val m = MusicPlayingFragment.instance( musicData.get( holder.adapterPosition) ,musicData.size )

        m.sharedElementEnterTransition = TransitionInflater.from(mContext).inflateTransition(R.transition.transition)
        m.enterTransition = TransitionInflater.from(mContext).inflateTransition(android.R.transition.explode)



        return m
    }

}

class AsyncDrawable (resource : Resources , bitmap : Bitmap ?, asyncImageDecorder: AsyncImageDecorder ) : BitmapDrawable(resource,bitmap)
{
    val weakReferenceTask : WeakReference<AsyncImageDecorder>
    init {
        weakReferenceTask = WeakReference( asyncImageDecorder )
    }
}


