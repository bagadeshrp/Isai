package com.baga.isai.showmusic

import android.widget.ImageView

object ImageTaskUtils
{
    /**
     * @param key It is the path at which bitmaps are stored
     *
     * returns true if a task is already running to load the image
     */
    fun cancelPotentialTasks(key : String , imageView: ImageView) : Boolean
    {

        val task = getBitmapWorkTask(imageView)

        if(task!=null){
            val path = task.path
            if(path != key)
            {
                task.cancel(true)
            }
            else
            {
                return false
            }
        }

        return true

    }

    fun getBitmapWorkTask( imageView: ImageView ? ) : AsyncImageDecorder ?
    {
        if(imageView==null)
        {
            return null
        }
        val drawable = imageView.drawable
        if( drawable is AsyncDrawable)
        {
            // from asyncdrawable get workertask
            return drawable.weakReferenceTask.get()
        }
        return null
    }
}