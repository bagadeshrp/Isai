package com.baga.isai.showmusic


import android.database.Cursor
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.baga.isai.Base.BaseFragment
import com.baga.isai.MainActivity

import com.baga.isai.R
import com.baga.isai.customcomp.PreCachingLayoutManager
import com.baga.isai.database.DatabaseUtil
import com.baga.isai.parsers.MusicCursorParser
import com.baga.isai.utils.GeneralUtils
import com.baga.isai.utils.ThemeUtil
import kotlinx.android.synthetic.main.fragment_music_list_view.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

class MusicListViewFragment : BaseFragment()
, Observer
{

    companion object
    {
        fun instance() : MusicListViewFragment
        {
            val m = MusicListViewFragment()
            return m
        }
    }

    var rootView : View ? = null
    lateinit var musicAdpater : MainRVAdapter
    private val toolbarTitle : String = "All Songs"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if(rootView!=null)
        {
            return rootView
        }
        rootView =  inflater.inflate(R.layout.fragment_music_list_view, container, false)
//        val toolbarLayout = rootView?.findViewById<FrameLayout>(R.id.main_toolbar)
//        val toolbar  = toolbarLayout?.findViewById<Toolbar>(R.id.toolbar)
//        if(toolbar!=null)
//        {
//            ( activity as MainActivity).initToolbar(toolbar,toolbarTitle)
//
//        }
//        else
//        {
//            Toast.makeText(context!!,"cannot",Toast.LENGTH_SHORT).show()
//        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateToolbarTheme()
        setupRecyclerview()
    }

    private fun setupRecyclerview() {
        val cursor  =  DatabaseUtil.getMusicFiles()
        if (cursor != null && context!=null) {
            musicAdpater= MainRVAdapter(context!!,MusicCursorParser.parse(cursor))
            val layout = PreCachingLayoutManager(context!!)
            layout.setExtraLayoutSpace( 1920)
//            Handler().postAtTime(Runnable {
//                layout.setExtraLayoutSpace( 0 )
//            },1000 )
            mainRecyclerView.layoutManager = layout
            mainRecyclerView.adapter = musicAdpater
        }

    }

    fun refresh()
    {
        setupRecyclerview()
    }

    override fun update(o: Observable?, arg: Any?) {
        if (o is ThemeUtil)
        {
            updateToolbarTheme()
        }
    }

    private fun updateToolbarTheme()
    {
        if (this.isVisible )
        {
            toolbarEditText?.setTextColor(ThemeUtil.currentTextColor)
            toolbarEditText?.setHintTextColor(ThemeUtil.currentTextColor)

            toolbarCardView?.setCardBackgroundColor(ThemeUtil.currentBackgroundColor)
            toolbar?.setBackgroundColor(ThemeUtil.currentTextColor)

        }

    }

    /**
     * add observer
     */
    override fun addObservers() {
        ThemeUtil.newInstance().addObserver(this)
    }

}
