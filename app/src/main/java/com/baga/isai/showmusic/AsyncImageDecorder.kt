package com.baga.isai.showmusic

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.os.AsyncTask
import android.widget.ImageView
import com.baga.isai.caching.MyLrucache
import com.baga.isai.models.Song
import com.baga.isai.utils.SongMetadata
import java.lang.ref.WeakReference



class AsyncImageDecorder (context: Context , imageView: ImageView ,val path : String,val key : String, val type : SongMetadata ) : AsyncTask<Void,Void,Bitmap>()
{

    val weakContext : WeakReference<Context>
    val weakImageView : WeakReference<ImageView>

    init {
        weakContext = WeakReference(context)
        weakImageView = WeakReference(imageView)
    }

    override fun doInBackground(vararg params: Void?): Bitmap? {

        if ( type==SongMetadata.ALBUM)
        {
            val bitmap = BitmapFactory.decodeFile(path)
            if (bitmap!=null)
            {
                MyLrucache.getInstance().set(key,bitmap)
            }
            return bitmap
        }
        else if (type == SongMetadata.SONG){
            val mediaMetadataRetriever = MediaMetadataRetriever()
            mediaMetadataRetriever.setDataSource(path)
            val data = mediaMetadataRetriever.embeddedPicture

            if (data!=null)
            {
                val bitmap =  BitmapFactory.decodeByteArray(data,0,data.size)
                MyLrucache.getInstance().set(key,bitmap)
                return bitmap
            }
        }

        return null
    }

    override fun onPostExecute(result: Bitmap?) {
        if(isCancelled)
        {
            return

        }
        if(result!=null )
        {
            val bitmapWorkertask  = ImageTaskUtils.getBitmapWorkTask( weakImageView.get() )
            if(this == bitmapWorkertask && weakImageView.get()!=null)
            {
                weakImageView.get()?.setImageBitmap(result)
            }
        }
    }
}