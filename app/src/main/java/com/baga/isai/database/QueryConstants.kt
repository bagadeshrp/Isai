package com.baga.isai.database
import com.baga.isai.database.DBConstants.Tables
import com.baga.isai.database.DBConstants.PlayListSongsColumns
import com.baga.isai.database.DBConstants.PlaylistColumns
import com.baga.isai.database.DBConstants.Tables.Companion.PLAYLISTS
import com.baga.isai.database.DBConstants.Tables.Companion.PLAYLIST_SONG

class QueryConstants
{
    interface PlayList
    {
        companion object
        {
            const val playlist = "create table if not exists ${PLAYLISTS}(${PlaylistColumns.id} INTEGER PRIMARY KEY AUTOINCREMENT,${PlaylistColumns.title} TEXT, ${PlaylistColumns.songCount} INTEGER, ${PlaylistColumns.totalDuration} TEXT ) "
            const val playlistSongs = "create table if not exists ${PLAYLIST_SONG}(${PlayListSongsColumns.id} INTEGER PRIMARY KEY AUTOINCREMENT,${PlayListSongsColumns.songId} TEXT, ${PlayListSongsColumns.playlistId} TEXT ) "
        }

    }
}