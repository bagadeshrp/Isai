package com.baga.isai.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.provider.MediaStore
import com.baga.isai.AppController
import com.baga.isai.database.MunDatabase.Companion.MUSIC_NOW_TABLE
import com.baga.isai.models.Album
import com.baga.isai.models.Song
import java.lang.Exception
import java.util.*

object DatabaseUtil
{
    fun getMusicFiles() : Cursor?
    {
        try {
            val contentResolver = AppController.getAppContext().contentResolver
            val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            val sortOder = MediaStore.Audio.Media.TITLE + " ASC"
            val cursor = contentResolver.query(uri,null,null,null,sortOder)
            cursor?.moveToFirst()
            if(cursor!=null && cursor.count >0)
            {
                return cursor
            }
            return null
        }
        catch (e : Exception)
        {
            return null
        }
    }

    fun changeCurrentPlaying(context: Context, song: Song)
    {
        val ins = MunDatabase.getInstance(context)
        val database = ins.writableDatabase
        val cursor  = database?.rawQuery(MunDatabase.MUSIC_NOW_SELECT,null)
        val cv = ContentValues()
        cv.put(MunDatabase.MUSIC_NOW_KEY,song.key)
        cv.put(MunDatabase.MUSIC_NOW_ALBUM,song.album)
        cv.put(MunDatabase.MUSIC_NOW_ARTIST,song.artist)
        cv.put(MunDatabase.MUSIC_NOW_DURATION,song.duration)
        cv.put(MunDatabase.MUSIC_NOW_PATH,song.path)
        cv.put(MunDatabase.MUSIC_NOW_TITLE,song.title)

        if (cursor!=null && cursor.count>0)
        {
            database.update(MunDatabase.MUSIC_NOW_TABLE,cv,"${MunDatabase.MUSIC_NOW_ID}=?", arrayOf("0"))
        }
        else
        {
            database?.insert(MUSIC_NOW_TABLE,null, cv)
        }

    }

    /**
     * "_id", "album", "artist", "artist_id", "numsongs", "minyear"
     *
     *
     */
    fun getAlbumFiles() : Cursor?
    {
        try {
            val contentResolver = AppController.getAppContext().contentResolver
            val uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI
            val sortOder : String  = MediaStore.Audio.Albums.DEFAULT_SORT_ORDER
            val projection = arrayOf(
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ARTIST_ID,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS,
                MediaStore.Audio.Albums.FIRST_YEAR,
                MediaStore.Audio.Albums.ALBUM_ART,
                MediaStore.Audio.Media.ALBUM_KEY

            )
            val cursor = contentResolver.query(uri,projection,null,null,sortOder)
            cursor?.moveToFirst()
            if(cursor!=null && cursor.count >0)
            {
                return cursor
            }
            return null
        }
        catch (e : Exception)
        {
            e.printStackTrace()
            return null
        }
    }


    fun getSongsFromAlbum( albumId : String ) : Cursor?
    {
        try {
            val contentResolver = AppController.getAppContext().contentResolver
            val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
//            val projection = arrayOf(
//                MediaStore.Audio.Media._ID,
//            )
            val sortOder : String  = MediaStore.Audio.Media.DEFAULT_SORT_ORDER
            val selection = MediaStore.Audio.Media.ALBUM_KEY+"=?"
            val selectionArg = arrayOf(albumId)
            val cursor  = contentResolver.query(uri,null,selection,selectionArg,sortOder)
            return cursor
        }
        catch ( e: Exception)
        {
            e.printStackTrace()
        }
        return null
    }

    /**
     *  return list of play list cursor
     */
    fun getPlayListCursor() : Cursor?
    {

//        val selection = " ${DBConstants.PlayListSongsColumns.playlistId} = ? "
//        val selectionArgs = arrayOf(playlistId)
        val orderBy = " ${DBConstants.PlaylistColumns.title} ASC "
        return getCursor(DBConstants.Tables.PLAYLISTS,null,null,null,null,null,orderBy)

    }

    /**
     *  Gets all songs for that plat list
     */
    fun getPlayListSongsCursor(playlistId : String) : Cursor?
    {

        val selection = " ${DBConstants.PlayListSongsColumns.playlistId} = ? "
        val selectionArgs = arrayOf(playlistId)
//        val orderBy = " ${DBConstants.PlayListSongsColumns.} ASC "
        return getCursor(QueryConstants.PlayList.playlistSongs,null,selection,selectionArgs,null,null,null)

    }



    private fun getCursor(table : String, columns : Array<String>? = null , selection : String ? = null, selectionArgs : Array<String> ? = null,groupBy : String?=null,having : String?=null,orderby : String?=null ): Cursor?
    {
      try {
          val ins = MunDatabase.getInstance(AppController.getAppContext())
          val cursor = ins.readableDatabase.query( table,columns,selection,selectionArgs,groupBy,having,orderby)
          if (cursor!=null && cursor.count>0)
          {
              return cursor
          }
          return null
      }
      catch (e : Exception)
      {
          e.printStackTrace()
      }
        return null
    }


    fun addNewPlayList( name : String)
    {
        val contentValues = ContentValues()
        val ins = MunDatabase.getInstance(AppController.getAppContext())
        if (isPlayListExists(name))
        {
            // update

            val where = "${DBConstants.PlaylistColumns.title} = ?"
            val selectionArgs = arrayOf(name)

            contentValues.put(DBConstants.PlaylistColumns.title,name)
            contentValues.put(DBConstants.PlaylistColumns.totalDuration,0)
            contentValues.put(DBConstants.PlaylistColumns.songCount,0)

            ins.writableDatabase.update(DBConstants.Tables.PLAYLISTS,contentValues,where,selectionArgs)


        }
        else
        {
            // create

            contentValues.put(DBConstants.PlaylistColumns.title,name)
            contentValues.put(DBConstants.PlaylistColumns.totalDuration,0)
            contentValues.put(DBConstants.PlaylistColumns.songCount,0)
            ins.writableDatabase.insert(DBConstants.Tables.PLAYLISTS,null,contentValues)
        }
    }

    private fun isPlayListExists( name : String) : Boolean
    {
        val selection = "${DBConstants.PlaylistColumns.title} = ?"
        val selectionArgs = arrayOf(name)
        val cursor = getCursor(DBConstants.Tables.PLAYLISTS,selection = selection, selectionArgs = selectionArgs)

        if (cursor!=null && cursor.count>0)
        {
            return true
        }
        return false

    }


}