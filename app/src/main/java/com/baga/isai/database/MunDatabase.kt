package com.baga.isai.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class MunDatabase (context: Context)  : SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION)
{

    companion object
    {
        val DATABASE_NAME = "BagaMun.db"
        val DATABASE_VERSION = 1

        val MUSIC_NOW_TABLE = "MusicNow"
        val MUSIC_NOW_ID = "_id"
        val MUSIC_NOW_KEY = "key"
        val MUSIC_NOW_TITLE = "title"
        val MUSIC_NOW_ALBUM= "album"
        val MUSIC_NOW_ARTIST= "artist"
        val MUSIC_NOW_DURATION = "duration"
        val MUSIC_NOW_PATH = "path"
        val MUSIC_NOW_CREATE = "create table if not exists ${MUSIC_NOW_TABLE} ( ${MUSIC_NOW_ID} integer PRIMARY KEY autoincrement,${MUSIC_NOW_KEY} text,${MUSIC_NOW_TITLE} text,${MUSIC_NOW_ARTIST} text,${MUSIC_NOW_DURATION} text,${MUSIC_NOW_PATH} text, ${MUSIC_NOW_ALBUM} text ) "
        val MUSIC_NOW_SELECT = "select * from ${MUSIC_NOW_TABLE}"



        private var instance : MunDatabase? = null
        fun getInstance(context: Context) : MunDatabase
        {
            var i = instance
            if ( i==null )
            {
                i = MunDatabase(context)
            }
            this.instance = i
            return i

        }

    }

    override fun onCreate(db: SQLiteDatabase?) {

        db?.apply {
            execSQL(MUSIC_NOW_CREATE)
            execSQL(QueryConstants.PlayList.playlist)
            execSQL(QueryConstants.PlayList.playlistSongs)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}
