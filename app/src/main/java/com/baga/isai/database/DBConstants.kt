package com.baga.isai.database

class DBConstants
{
    interface Tables
    {
        companion object
        {
            const val PLAYLISTS = "Playlists"
            const val PLAYLIST_SONG = "PlaylistSongs"
        }

    }


    interface PlaylistColumns
    {
        companion object
        {
            const val id = "ID"
//            const val playlistId = "PLAYLIST_ID"
            const val title= "TITLE"
            const val songCount= "SONG_COUNT"
            const val totalDuration= "TOTAL_TIME"

        }

    }


    interface PlayListSongsColumns
    {
        companion object
        {
            const val id = "ID"
            const val songId = "SONG_ID"
            const val playlistId = "PLAYLIST_ID"
        }
    }


}