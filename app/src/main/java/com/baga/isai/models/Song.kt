package com.baga.isai.models

import android.os.Parcel
import android.os.Parcelable

/**
 *
 *
 * val title = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
val artist = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
val album = lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM))
val duration = GeneralUtils.getTimeInMMSS( lCursor.getString(lCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)).toLong()  )
val path = lCursor.getString(lCursor.getColumnIndex(MediaStore.MediaColumns.DATA))
val key = lCursor.getString(lCursor.getColumnIndex(MediaStore.MediaColumns._ID))

 *
 * @param key  is MediaStore.MediaColumns._ID
 * @param path is MediaStore.MediaColumns.DATA)
 * @param title is MediaStore.Audio.Media.TITLE
 * @param album is MediaStore.Audio.Media.ALBUM
 * @param number stating index is 1
 *
 */
data class Song constructor(
    val number : Int,
    var title : String ,
    var artist : String ,
    var path : String ,
    var key : String ,
    var duration : String,
    var album : String,
    val durationInMs : Long
) : Parcelable
{
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(number)
        dest.writeString(title)
        dest.writeString(artist)
        dest.writeString(path)
        dest.writeString(key)
        dest.writeString(duration)
        dest.writeString(album)
        dest.writeLong(durationInMs)
    }

    override fun describeContents(): Int {
        return 0;
    }
    constructor( parcel : Parcel): this
        (
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong()

    )
    companion object{

        @JvmField
        val CREATOR : Parcelable.Creator<Song> = object : Parcelable.Creator<Song>{
            override fun createFromParcel(source: Parcel): Song {
                return Song(source)
            }

            override fun newArray(size: Int): Array<Song?> {
                return arrayOfNulls<Song>(size)
            }
        }
    }
}
