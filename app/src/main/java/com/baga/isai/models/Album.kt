package com.baga.isai.models

/**
 * MediaStore.Audio.Media._ID
 * MediaStore.Audio.Media.ALBUM
 * MediaStore.Audio.Media.ARTIST
 * MediaStore.Audio.Media.ARTIST_ID
 * MediaStore.Audio.Albums.NUMBER_OF_SONGS
 * MediaStore.Audio.Albums.FIRST_YEAR
 * MediaStore.Audio.Media.DATA

 */
data class Album (
    val id : Long,
    val title : String,
    val artistName : String,
    val artistId : Long,
    val songCount : Int,
    val year : Int,
    val path : String?,
    val albumId : String
)