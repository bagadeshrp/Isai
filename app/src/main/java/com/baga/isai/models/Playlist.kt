package com.baga.isai.models

data class Playlist(
    val playlistId : Int,
    val playlistTitle : String,
    val songCount : Int,
    val duration : String

)