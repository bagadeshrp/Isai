package com.baga.isai

import android.app.Application
import android.content.Context
import com.baga.isai.utils.ThemeUtil

class AppController : Application()
{
    init {
        instance=this
    }
    interface BOTTOM_SHEET
    {
        companion object
        {
            // bottom sheet
            val MORE = "More"
            val SHARE=  "Share"
            val SETTINGS = "Settings"
            val DARKMODE = "Dark Mode"
            val RATE = "Rate the app"
            val LIBRARY  = "Library"
        }

    }

    companion object {
        private lateinit var instance : AppController
        val KEY_TOTAL_COUNT = "Key total count"

        // music constants

        val IS_MUSIC_PL = "IsMusicPlaying"

        // system notification
        val NOTIFICATION_CHANNEL = "ISAI_NOT"
        val NOTIFCATION_ID = 192


        //fragment

        val ALBUM_CONTENT_FRAG = "albumContentFragment"



        val SONG_PARCEL = "parcelingSong"



        fun getAppContext() : Context
        {
            return instance.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        ThemeUtil.initialize()
    }
}
