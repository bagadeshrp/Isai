package com.baga.isai.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.baga.isai.AppController
import com.baga.isai.MainActivity
import com.baga.isai.R

class NotificationPanel (val mContext : Context ) {

    private val notificationManager : NotificationManager = mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val builder : NotificationCompat.Builder = NotificationCompat.Builder(mContext,AppController.NOTIFICATION_CHANNEL)

        .setOngoing(false) // Clear all in notification will be clear this notification
        .setPriority(NotificationCompat.PRIORITY_MIN)
        .setSound(null)

    val remoteView : RemoteViews=  RemoteViews(mContext.packageName,R.layout.notifi_view)
    val smallRemoteView : RemoteViews=  RemoteViews(mContext.packageName,R.layout.noti_small_view)

    private val intent = Intent(mContext,MainActivity::class.java)
    private val pendingIntent = PendingIntent.getActivity(mContext,1010,intent,PendingIntent.FLAG_UPDATE_CURRENT)


    fun getBuild( title : String?="" , artist : String?="") : NotificationCompat.Builder
    {
//        builder.setContent(smallRemoteView)
//        builder.setCustomBigContentView(remoteView)

        builder.apply {
            setContentTitle(title)
            setContentText(artist)
            setSmallIcon(R.drawable.ic_menu2icon)
            setLargeIcon(BitmapFactory.decodeResource(mContext.resources,R.drawable.ic_menuicon))
            addAction(R.drawable.ic_prebutton1,"ee",null)
            addAction(R.drawable.ic_playbutton2,"ee",null)
            addAction(R.drawable.ic_nextbutton1,"ee",null)
        }



        builder.setContentIntent(pendingIntent)
        return builder
    }


    fun createNotificationChannel(m : NotificationManager?)
    {
        if (Build.VERSION.SDK_INT> Build.VERSION_CODES.O)
        {
            val name = "Notification name"
            val description = "Description text"
            val importance = NotificationManager.IMPORTANCE_MIN
            val channel = NotificationChannel(AppController.NOTIFICATION_CHANNEL,name,importance).apply {
                enableVibration(true)
                setDescription(description)
                setSound(null,null)
                enableLights(true)

            }
            m?.createNotificationChannel(channel)
        }
    }


}